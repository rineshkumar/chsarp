﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._022_CS6NewFeatures
{
    public class _005_usingawaitInCatchAndFinally
    {
        static void Main(string[] args)
        {
            //Await can not be used in catch block for asynchronous 
            //exception handling 
            /*
              try
               {
                  Processor processor = new Processor();
                  await processor.ProccessAsync();
               }
               catch (Exception exception)
               {
                  ExceptionLogger logger = new ExceptionLogger();
                  // Catch operation also can be aync now!!
                  await logger.HandleExceptionAsync(exception);
               }
             */
        }
    }
}
