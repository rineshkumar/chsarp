﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._022_CS6NewFeatures
{
    public class _002_ConditionalAccessOperator
    {
        //Used for simplification of null checks (?.?.??)
        //Old Style
        /*
         * if (employee != null && employee.EmployeeProfile!= null)
            employeeMiddleName = employee.EmployeeProfile.MiddleName;
         */
        //new Type 
        /*
         string employeeMiddleName =
        employee?.EmployeeProfile?.MiddleName ?? "N/A"; //N/A is the default value
        //?.?.??
        */
    }
}
