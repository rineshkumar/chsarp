﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._022_CS6NewFeatures
{
    public class _001_ExpressionBodiedMethod
    {
        public void ShowMessage() => Console.WriteLine("Hello World");
        public static void Main(string[] s) {
            _001_ExpressionBodiedMethod ebm = new _001_ExpressionBodiedMethod();
            ebm.ShowMessage();
            Console.ReadLine();
        }
    }
}
