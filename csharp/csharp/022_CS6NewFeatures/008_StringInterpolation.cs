﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._022_CS6NewFeatures
{
    public class _008_StringInterpolation
    {
        public static void Main(string[] args)
        {
            string hello = "Hello";
            string world = "World!";
            Console.WriteLine($"{hello}  {world}");
            Console.ReadLine();
        }
    }
}
