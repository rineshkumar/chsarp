﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._022_CS6NewFeatures
{
    public class _006_ExceptionFilters
    {
        /*
         * We can filter exception which we want to handle in the exception filters
         * 
         */
        public static void Main(string[] args)
        {
            try
            {

            }
            catch (Exception ex )
            {
                if (ex.GetType() != typeof(SqlException))
                {
                    // Do logging 
                }
                else
                {
                    //else leave it to the calling method to decide.
                    throw;
                }
                
            }
        }
    }
}
