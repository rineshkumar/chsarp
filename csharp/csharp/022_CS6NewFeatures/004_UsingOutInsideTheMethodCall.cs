﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._022_CS6NewFeatures
{
    public class _004_UsingOutInsideTheMethodCall
    {
        static void Main(string[] args)
        {
            MethodWithOut("10");
            MethodWithOut("5");
            MethodWithOut("A");
            Console.ReadLine();
        }

        private static void MethodWithOut(string value)
        {
            if (int.TryParse(value, out int parsedValue) && parsedValue > 5) // Using out
            {
                Console.WriteLine("Its integer");
            }
            else {
                Console.WriteLine("Its not integer");
            }
        }
    }
}
