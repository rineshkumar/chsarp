﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    class _003_UpdateQueries
    {
        static void Main1(string[] args)
        {
            using (var db = new AdventureWorksDataContext())
            {
                var store = db.Stores.Single(s => s.StoreCode == "RKS6");
                store.Name = "Rk store 66";
                db.SubmitChanges();
            }
            Console.WriteLine("Done");
            Console.Read();
        }
    }
}
