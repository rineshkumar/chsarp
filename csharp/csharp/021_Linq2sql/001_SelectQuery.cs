﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    class _001_SelectQuery
    {
        static void Main1(string[] args)
        {
            using (var db = new AdventureWorksDataContext())
            {
                //-----------------------------
                //selecting customers
                //db.Customers.ToList().ForEach(
                //    c => { Console.WriteLine(c.rowguid+" "+c.AccountNumber); }
                //    );
                //-----------------------------
                //selecting products 
                //db.Products.Take(10).ToList().ForEach(
                //    p => { Console.WriteLine(p.ProductNumber + " " + p.Name); }
                //    );
                //-----------------------------
                //Using condition 
                //db.Products.Where(p => p.ProductID == 356).ToList().ForEach(
                //    p => { Console.WriteLine(p.ProductID + " " + p.Name); }
                //    );
                //-----------------------------
                //using multiple  condition 
                //db.Products.Where(p => p.ProductID == 356 && p.ReorderPoint == 600 ).ToList().ForEach(
                //    p => { Console.WriteLine(p.ProductID + " " + p.Name); }
                //    );
                //-----------------------------
                //using range of values 
                //db.Products.Where(
                //        p => new[] { 1, 2, 3, 4 }.Contains(p.ProductID)) // Defining range 
                //        .ToList()
                //        .ForEach(p => { Console.WriteLine(p.ProductID + " " + p.Name + " " + p.ProductNumber); });
                //-----------------------------
                //Ordering of data 
                //db.Products.Take(20).OrderBy(p => p.Name).ToList().ForEach(p => { Console.WriteLine(p.Name); });
                //-----------------------------
                ///like query 
                //db.Products.Take(20).Where(p => p.Name.Contains("Cran")).ToList().ForEach(p => { Console.WriteLine(p.Name); });
                //-----------------------------
                //Between query
                // db.Products.Where(p => p.ProductID >= 316 && p.ProductID <= 350).ToList().ForEach(p => { Console.WriteLine(p.ProductID + " " + p.Name + " " + p.ProductNumber); });
                //-----------------------------
                //Group by query 
                //db.SalesOrderDetails.Where(d => new int[] { 43660, 43670, 43672 }.Contains(d.SalesOrderID))
                //    //.ToList()
                //    //.ForEach(d => Console.WriteLine(d.SalesOrderID+" "+d.SalesOrderDetailID));
                //    .GroupBy(d => d.SalesOrderID) // Group = key + colletion of salesorderdetail objects
                //    .Select(g => new { orderid = g.Key, salesOrderDetails = g.Select(sod => sod) })
                //    .ToList()
                //    .ForEach(
                //    n =>
                //    {
                //        Console.WriteLine("Orderid is ==" + n.orderid);
                //        n.salesOrderDetails.ToList().ForEach(d => Console.WriteLine(d.SalesOrderDetailID+" " +d.CarrierTrackingNumber));
                //    });
                //-----------------------------
                //Group by with aggregate - Count 
                //db.SalesOrderDetails.Where(d => new int[] { 43660, 43670, 43672 }.Contains(d.SalesOrderID))
                //    //.ToList()
                //    //.ForEach(d => Console.WriteLine(d.SalesOrderID+" "+d.SalesOrderDetailID));
                //    .GroupBy(d => d.SalesOrderID) // Group = key + colletion of salesorderdetail objects
                //        .Select(g => new { orderid = g.Key, salesOrderDetails = g.Select(sod => sod) })
                //    .ToList()
                //    .ForEach(
                //    n =>
                //    {
                //        Console.WriteLine("Orderid is ==" + n.orderid);
                //        Console.WriteLine("number of sales order deails  -- " + n.salesOrderDetails.Count());

                //    });
                //-----------------------------
                //Group by with aggregate - Sum
                //db.SalesOrderDetails.Where(d => new int[] { 43660, 43670, 43672 }.Contains(d.SalesOrderID))
                //    //.ToList()
                //    //.ForEach(d => Console.WriteLine(d.SalesOrderID+" "+d.SalesOrderDetailID));
                //    .GroupBy(d => d.SalesOrderID) // Group = key + colletion of salesorderdetail objects
                //        .Select(g => new { orderid = g.Key, totalOrders = g.Average(sod => sod.OrderQty) })
                //    .ToList()
                //    .ForEach(
                //    n =>
                //    {
                //        Console.WriteLine("Orderid is ==" + n.orderid);
                //        Console.WriteLine("Total Order Quantity -- " + n.totalOrders);

                //    });
                //-----------------------------
                //Group by using multiple fields 
                //db.SalesOrderHeaders
                //    .Where(soh => soh.CustomerID <= 11010)
                //    .GroupBy(soh => new { soh.CustomerID, soh.SalesPersonID })
                //    .Select(g => new
                //    {
                //        customerid = g.Key.CustomerID,
                //        salesPersonId = g.Key.SalesPersonID,
                //        soh = g.Select(x => x)

                //    })
                //    .ToList()
                //    .ForEach(go => {
                //        Console.WriteLine("CustomerId = "+go.customerid);
                //        Console.WriteLine("SalesPersonId = "+go.salesPersonId);
                //        Console.WriteLine("count "+go.soh.Count());
                //    });
                //-----------------------------
                //Group by and having 
                //db.SalesOrderDetails.Where(d => new int[] { 43660, 43670, 43672 }.Contains(d.SalesOrderID))
                //    //.ToList()
                //    //.ForEach(d => Console.WriteLine(d.SalesOrderID+" "+d.SalesOrderDetailID));
                //    .GroupBy(d => d.SalesOrderID) // Group = key + colletion of salesorderdetail objects
                //    .Where(g => g.Sum(sod => sod.OrderQty) > 5) // Having -- Adding condition to the aggregate 
                //    .Select(g => new { orderid = g.Key, totalOrders = g.Sum(sod => sod.OrderQty) })
                //    .ToList()
                //    .ForEach(
                //    n =>
                //    {
                //        Console.WriteLine("Orderid is ==" + n.orderid);
                //        Console.WriteLine("Total Order Quantity -- " + n.totalOrders);

                //    });
                //-----------------------------
                //Using distinct 
                db.SalesOrderDetails
                    .Where(sod => sod.OrderQty > 30)
                    .Select(sod => sod.ProductID)
                    .Distinct()
                    .ToList()
                    .ForEach(pid => { Console.WriteLine(pid); });
                Console.Read();
            }
        }
    }
}
