﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace csharp._021_Linq2sql
{
    class _009_Transactions
    {
        static void Main(string[] args)
        {
            using (var db = new AdventureWorksDataContext())
            {
                //--------------------------------------
                ///Using transaction scope for atomicity
                using (var t = new TransactionScope())
                {
                    var store = new Store() { StoreCode = "RKS6", Name = "Rk store 6 ", City = "Bangalore", State = "KA", Zip = "56010" };
                    store.Sales.Add(new Sale() { OrderNumber = "TestOrder5", StoreCode = "RKS456", OrderDate = DateTime.Now, Quantity = 20, TitleID = 12345, Terms = "Hello 3 " });
                    db.Stores.InsertOnSubmit(store);
                    db.SubmitChanges();
                    t.Complete();
                }
            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
