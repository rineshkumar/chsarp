﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    class _008_callingFunctions
    {
        static void Main1(string[] args)
        {
            using (var db = new AdventureWorksDataContext())
            {
                Console.WriteLine(db.AveragePrice());
            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
