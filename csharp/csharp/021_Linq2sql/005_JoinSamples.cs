﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    class _005_JoinSamples
    {
        static void Main1(string[] args)
        {

            using (var db = new AdventureWorksDataContext())
            {
                //-----------------------------
                //Inner join example
                //db.SpecialOffers.
                //    Join(
                //        db.SpecialOfferProducts,
                //        so => so.SpecialOfferID,
                //        sop => sop.SpecialOfferID,
                //        (so, spo) => new { id = so.SpecialOfferID, description = so.Description , ProductId = spo.ProductID })
                //        .Where(x => x.id == 2)
                //        .ToList()
                //        .ForEach(x => {
                //            Console.WriteLine(x.id+" "+x.description+" " +x.ProductId);
                //        });

                //-----------------------------
                //Left join example
                //GroupJoin does the left outer join
                //db.Stores.
                //    GroupJoin(db.Sales,
                //                st => st.StoreCode,
                //                sa => sa.StoreCode,
                //                (st, sa) => new { st, sa })
                //    .SelectMany(
                //        g => g.sa.DefaultIfEmpty(),//Collection Selector 
                //        //x is the input group , y i element from the previous line expression
                //        (x, y) => new {st = x.st , sa = y  } // result selecter
                //    )
                //    .ToList()
                //    .ForEach(x =>
                //    {
                //        Console.WriteLine(x.st.StoreCode
                //                                        + "~"
                //                                        + x.st.Name
                //                                        + "~"
                //                                        + x.st.Address
                //                                        + "~"
                //                                        + x.st.City
                //                                        + "~"
                //                                        + x.st.State
                //                                        + "~"
                //                                        + x.st.Zip
                //                                        + "=="

                //                                        );

                //        var item = x.sa;
                //        if (item != null) {
                //            Console.WriteLine("-->" + item.OrderNumber
                //            + "~"
                //            + item.StoreCode
                //            + "~"
                //            + item.OrderDate
                //            + "~"
                //            + item.Quantity
                //            + "~"
                //            + item.Terms
                //            + "~"
                //            + item.TitleID
                //            );
                //        }
                //        Console.WriteLine("*************************");
                //    }
                //);
                //-----------------------------------
                //Performing cartesian product 
                db.Stores
                    .SelectMany(s => db.Sales, (st, sa) => new { store = st.StoreCode, orderCode = sa.OrderNumber })
                    .ToList()
                    .ForEach(data => Console.WriteLine(data.store + " " + data.orderCode));
            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
