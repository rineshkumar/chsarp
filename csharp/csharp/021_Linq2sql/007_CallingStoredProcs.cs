﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    class _007_CallingStoredProcs
    {
        static void Main1(string[] args)
        {
            using (var db = new AdventureWorksDataContext())
            {
                var store = db.getStoreDetails1("RKS1").Single(); ;
                Console.WriteLine(store.Name);
            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
