﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    class AdventureWorksDbHandler
    {
        static void Main1(string[] args)
        {
            using (var db = new AdventureWorksDataContext()) {
                //Getting list of 100 products from table
                //var products = db.Products.Take(100);
                //foreach (var item in products)
                //{
                //    Console.WriteLine(item.Name);
                //}
                //------------------------
                //Getting list of products with name starting with A 
               //var filteredProducts =  db.Products.Where(p => p.Name.ToLower().StartsWith("a"));
               //filteredProducts.ToList().ForEach(p => {
               //    Console.WriteLine(p.Name);
               //});
                //------------------------------
                //Adding data to data base 
                
                //var productCategory = new ProductCategory() { Name = "sample2", ModifiedDate = DateTime.Now, rowguid = Guid.NewGuid()};
                //db.ProductCategories.InsertOnSubmit(productCategory);
                //db.SubmitChanges();
                //------------------------------
                //Updating data in database 
                //var category = db.ProductCategories.Single(pc => pc.Name.ToLower().Equals("sample"));
                //category.rowguid = Guid.NewGuid();//Updating data 
                //db.SubmitChanges();
                //------------------------------
                //Deleting data from database 
                //var category = db.ProductCategories.Single(pc => pc.Name.ToLower().Equals("sample"));
                //db.ProductCategories.DeleteOnSubmit(category);
                //db.SubmitChanges();



            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
