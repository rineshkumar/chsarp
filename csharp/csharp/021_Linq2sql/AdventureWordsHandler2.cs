﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    public class AdventureWordsHandler2
    {
        static void Main1(string[] args)
        {
            //Getting a handle on the database 
            //Data Source=.;Initial Catalog=AdventureWorks2012;Integrated Security=True
            DataContext db = new DataContext("Data Source=.;Initial Catalog=AdventureWorks2012;Integrated Security=True");
            //--------------------------------------
            //Reading from a store object 
            //var stores = db.GetTable<Store>();
            //var firstFiveStore = stores.Take(5);
            
            //foreach (var store in firstFiveStore)
            //{
            //    Console.WriteLine(store.Name);
            //    ///Reading sales data from store object 
            //    foreach (var item in store.Sales)
            //    {
            //        Console.WriteLine(item.StoreCode);
            //    }
            //}
            //--------------------------------------
            //Updating the content of a table
            //var stores = db.GetTable<Store>();
            //var store = stores.Single(r => r.StoreCode == "TST2");
            //store.Name = "Rinesh = Store 1";
            //foreach (var item in store.Sales)
            //{
            //    item.Quantity = 20;
            //    item.Terms = "Hello World ";
            //}
            //db.SubmitChanges();
            //--------------------------------------
            
            
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
