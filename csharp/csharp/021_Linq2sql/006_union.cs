﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    class _006_union
    {
        static void Main1(string[] args)
        {
            //-----------------------------------
            
            using (var db = new AdventureWorksDataContext())
            {
                var set1 = db.Stores.Where(s => new string[] { "RKS1", "RKS2" }.Contains(s.StoreCode));
                var set2 = db.Stores.Where(s => new string[] { "RKS3", "RKS2" }.Contains(s.StoreCode));
                //Union of records 
                //set1.Union(set2).ToList().ForEach(s => Console.WriteLine(s.StoreCode));
                //Interesection of records 
                set1.Intersect(set2).ToList().ForEach(s => Console.WriteLine(s.StoreCode));
            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
