﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    class _004_deleteQueries
    {
        static void Main1(string[] args)
        {
            using (var db = new AdventureWorksDataContext())
            {
                //-----------------------------
                //Simle delete
                //var store = db.Stores.Single(s => s.StoreCode == "RKS6");
                //db.Stores.DeleteOnSubmit(store);
                //db.SubmitChanges();
                //-----------------------------
                //cascading delete 
                var store = db.Stores.Single(s => s.StoreCode == "RKS5");
                foreach (var sale in store.Sales) {
                    db.Sales.DeleteOnSubmit(sale);
                }
                db.Stores.DeleteOnSubmit(store);
                db.SubmitChanges();
            }
            Console.WriteLine("Done");
            Console.Read();
        }
    }
}
