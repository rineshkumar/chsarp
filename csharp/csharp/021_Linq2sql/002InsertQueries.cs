﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._021_Linq2sql
{
    class _002InsertQueries
    {
        static void Main1(string[] args)
        {
            using (var db = new AdventureWorksDataContext()) 
            {
                //-----------------------------
                //Simple insert to main and related table 
                //var store = new Store() { StoreCode = "RKS3", Name = "Rk store 3 ", City = "Bangalore", State = "KA", Zip = "56010" };
                //store.Sales.Add(new Sale() { OrderNumber = "TestOrder33", StoreCode = "RKS3", OrderDate = DateTime.Now, Quantity = 20, TitleID = 1234, Terms = "Hello 3 " });
                //db.Stores.InsertOnSubmit(store);
                //db.SubmitChanges();
                //-----------------------------
                //Adding multiple rows

                var store1 = new Store() { StoreCode = "RKS5", Name = "Rk store 5", City = "Bangalore", State = "KA", Zip = "56010" };
                var store2 = new Store() { StoreCode = "RKS6", Name = "Rk store 6", City = "Bangalore", State = "KA", Zip = "56010" };
                db.Stores.InsertOnSubmit(store1);
                db.Stores.InsertOnSubmit(store2);
                db.SubmitChanges();
            }
            Console.WriteLine("Done");
            Console.ReadLine();

        }
    }
}
