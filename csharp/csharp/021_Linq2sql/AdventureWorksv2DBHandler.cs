﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csharp._021_Linq2sqlV2;
namespace csharp._021_Linq2sqlV2
{
    public class AdventureWorksv2DBHandler
    {
        static void Main1(string[] args)
        {
            AdventureWorksV2DataContext db = new AdventureWorksV2DataContext();
            
            //---------------------------
            //Creating an entry 
            //var store = new Store() { StoreCode = "RKS2", Name = "Rk store 1 ", City = "Bangalore", State = "KA", Zip = "56010" };
            //store.Sales.Add(new Sale() { OrderNumber = "TestOrder4", StoreCode = "RKS1", OrderDate = DateTime.Now, Quantity = 20, TitleID = 1234, Terms = "Hello 2 " });
            //db.Stores.InsertOnSubmit(store);
            //db.SubmitChanges();
            //---------------------------
            //Reading an entry 
            //var store = db.Stores.Single(s => s.StoreCode == "RKS2");
            //Console.WriteLine(store.StoreCode+" "+store.Name+" " +store.City+" "+store.State+" "+store.Zip);
            //store.Sales.ToList().ForEach(s => { Console.WriteLine(s.OrderNumber+" " +s.StoreCode+" " +s.OrderDate+" "+s.Quantity+" " +s.Terms+" "+s.TitleID); });
            //---------------------------
            //Updating an entry 
            //store.Name = "RK store 2";
            //store.Sales.ToList().ForEach(s => s.Terms = "Hello2 ");
            //db.SubmitChanges();
            //---------------------------
            //Deleting an entry
            //db.Sales.DeleteOnSubmit(store.Sales.Single(s => s.StoreCode == store.StoreCode));
            //db.Stores.DeleteOnSubmit(store);
            //db.SubmitChanges();
            //---------------------------
            //Navigting from child to parent . 
            db.Sales.ToList().ForEach(
                s => { Console.WriteLine(s.Store.StoreCode+" "+s.Store.Name); }
                );
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
