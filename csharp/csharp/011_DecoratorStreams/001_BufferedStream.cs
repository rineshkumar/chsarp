﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._011_DecoratorStreams
{
    class _001_BufferedStream
    {
        static void Main1(string[] args)
        {
            writeToFile();
            ReadFromFile();
            Console.ReadLine();
        }

        private static void ReadFromFile()
        {
            FileStream fs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt", FileMode.Open);
            BufferedStream bs = new BufferedStream(fs);
            byte[] b = new byte[bs.Length];
            bs.Read(b, 0, (int)bs.Length);
            string code = Encoding.Unicode.GetString(b);
            Console.WriteLine(code);
        }

        private static void writeToFile()
        {
            using (FileStream fs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt", FileMode.Create))
            {
                using (BufferedStream bs = new BufferedStream(fs))
                {
                    string message = "HelloWorld ";
                    byte[] b = Encoding.Unicode.GetBytes(message);
                    bs.Write(b, 0, (int)b.Length);
                }
            }
        }
    }
}
