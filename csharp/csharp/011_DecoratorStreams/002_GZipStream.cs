﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
namespace csharp._011_DecoratorStreams
{
    class _002_GZipStream
    {
        static void Main1(string[] args)
        {
            CompressFile();
            decompressFile();
        }

        private static void decompressFile()
        {
            using (FileStream cfs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt.gz", FileMode.Open))
            {
                using (FileStream dfs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sampleUnzipped.txt", FileMode.Append)) 
                {
                    using (GZipStream gs = new GZipStream(cfs, CompressionMode.Decompress))
                    {
                        gs.CopyTo(dfs);
                    }
                }
            }
        }

        private static void CompressFile()
        {
            using (FileStream ifs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt", FileMode.Open))
            {
                using (FileStream cfs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt.gz", FileMode.Create))
                {
                    using (GZipStream gs = new GZipStream(cfs, CompressionMode.Compress))
                    {
                        ifs.CopyTo(gs);
                    }
                }
            }
        }
    }
}
