﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
namespace csharp._011_DecoratorStreams
{
    class _003_DeflateStream
    {
        static void Main1(string[] args)
        {
            compressFile();
            decompressFile();
        }

        private static void decompressFile()
        {
            using (FileStream cfs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt.cmp", FileMode.Open))
            {
                using (FileStream ofs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sampledeflate.txt", FileMode.Create))
                {
                    using (DeflateStream dfs = new DeflateStream(cfs, CompressionMode.Decompress)) 
                    {
                        dfs.CopyTo(ofs);
                    }
                }
            }
        }

        private static void compressFile()
        {
            using (FileStream ifs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt", FileMode.Open))
            {
                using (FileStream cfs = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt.cmp", FileMode.Create))
                {
                    using (DeflateStream dfs = new DeflateStream(cfs,CompressionMode.Compress))
                    {
                        ifs.CopyTo(dfs);
                    }
                }
            }
        }
    }
}
