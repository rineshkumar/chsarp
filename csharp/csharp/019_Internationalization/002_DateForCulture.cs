﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._020_DateHandling
{
    class _002_DateForCulture
    {
        static void Main1(string[] args)
        {
            DateTime dt = DateTime.Now;
            CultureInfo c = new CultureInfo("fr-Fr");
            CultureInfo d = new CultureInfo("de-DE");
            Console.WriteLine(dt.ToString("d"));
            Console.WriteLine(dt.ToString("d", c));
            Console.WriteLine(dt.ToString("d", d)); 
            Console.ReadLine();
        }
    }
}
