﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Resources;
using System.Reflection;
using System.Threading;
namespace csharp._019_Internationalization
{
    class _001_ReadFromResourceFIle
    {
        static void Main1(string[] args)
        {
            Assembly a  = Assembly.GetExecutingAssembly();
            ResourceManager rm = new ResourceManager("csharp._019_Internationalization.messages", a);
            Console.WriteLine(rm.GetString("message"));
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("fr-FR");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("fr-FR");
            Console.WriteLine(rm.GetString("message"));
            Console.ReadLine();

        }
    }
}
