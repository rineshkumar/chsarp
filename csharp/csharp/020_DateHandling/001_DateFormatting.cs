﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._020_DateHandling
{
    class _001_DateFormatting
    {
        static void Main1(string[] args)
        {
            //http://www.csharp-examples.net/string-format-datetime/
            DateTime dt = DateTime.Now;
            var a = string.Format("{0:y yy yyy yyyy}",dt);
            Console.WriteLine(a);
            var b = string.Format("{0:M MM MMM MMMM}", dt);
            Console.WriteLine(b);
            var c = string.Format("{0:d dd ddd dddd}", dt);
            Console.WriteLine(c);
            var d = string.Format("{0:h hh H HH}", dt);
            Console.WriteLine(d);
            var e = string.Format("{0:m mm}", dt);
            Console.WriteLine(e);
            var f = string.Format("{0:s ss}", dt);
            Console.WriteLine(f);
            var g = string.Format("{0:t tt}", dt); // AM : PM
            Console.WriteLine(g);
            var h = string.Format("{0:z zz zzz}", dt); // Time Zone +5 +05 +05.30
            Console.WriteLine(h);
            var i = string.Format("{0:d/MM/yyyy HH:mm:ss}", dt);
            Console.WriteLine(i);
            Console.ReadLine();
        }
    }
}
