﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _005_ThreadWithException
    {
        /*
         * Exception thrown in a thread is never caught in the main thread 
         */
        static void Main1(string[] args)
        {
            try
            {
                Thread t1 = new Thread(MethodWithException);
                t1.Start();
                t1.Join();
            }
            catch (Exception)
            {

                throw;
            }

            Console.WriteLine("Executon completed");
            Console.ReadLine();
        }

        private static void MethodWithException(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
