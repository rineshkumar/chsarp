﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._001_threads
{
    class _003_ThreadYield
    {
        static void Main1(string[] args)
        {
            Thread t1 = new Thread(YieldingThread);
            Thread t2 = new Thread(NonYiendingThread);
            t1.Start(); t2.Start();
            t1.Join();
            t2.Join();
            Console.WriteLine("Main execution completed ");
            Console.ReadLine();
        }

        private static void NonYiendingThread(object obj)
        {
            Console.WriteLine("Non Yielding thread executed ");
        }

        private static void YieldingThread(object obj)
        {
            Console.WriteLine("Going to yield ");
            Thread.Yield();//If commented t2 executes first . . Not sure why !!
            Console.WriteLine("Statement after yielding");
        }
    }
}
