﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _002_ThreadSleep
    {
        static void Main1(string[] args)
        {
            Thread t1 = new Thread(WaitForSomeTime);
            t1.Start();
            t1.Join();
            Console.WriteLine("Main thread execution completed.");
            Console.ReadLine();
        }

        private static void WaitForSomeTime(object obj)
        {
            /*
             * 0 - Relinquishes the threads current time slice immediately. 
             * Thread.Yield - Relinqusihes the current threads time slice, but to thread running in the same processor
             * Thread is blocked when sleeping or waiting.
             * When a thread unblocks, OS performs a context switch 
             */
            //Thread.Sleep(0);
            Thread.Sleep(3000);//3 seconds 
            Console.WriteLine("Wait function execution completed");
        }
    }
}
