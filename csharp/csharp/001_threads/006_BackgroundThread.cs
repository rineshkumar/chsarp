﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _006_BackgroundThread
    {
        /*
         * If the application completes execution, it will not wait for the background threads to complete.     
         */
        static void Main1(string[] args)
        {
            Thread t1 = new Thread(DoSomeThing);
            t1.IsBackground = true;
            //  t1.Join(); //Will throw error if uncommented since the background thread has not started
            Console.WriteLine("Completed");
            Console.ReadLine();
        }

        private static void DoSomeThing(object obj)
        {
            Console.WriteLine("Thead executing ");
        }
    }
}
