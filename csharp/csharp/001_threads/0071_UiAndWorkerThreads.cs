﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _0071_UiAndWorkerThreads
    {
        public string SharedVariable1 { get; set; }
        public string SharedVariable2 { get; set; }
        public string SharedVariable3 { get; set; }

        static void Main(string[] args)
        {
            _0071_UiAndWorkerThreads tt = new _0071_UiAndWorkerThreads();
            tt.CompleteTimeComsumingWork();
            tt.ShowData();
            Console.ReadLine();
        }

        private void ShowData()
        {
            Console.WriteLine("SharedVariable1 " + SharedVariable1);
            Console.WriteLine("SharedVariable2 " + SharedVariable2);
            Console.WriteLine("SharedVariable3 " + SharedVariable3);
        }

        private void CompleteTimeComsumingWork()
        {
            var t1 = new Thread(() =>
             {
                 Thread.Sleep(TimeSpan.FromSeconds(5));
                 SharedVariable1 = "One";

             });
            var t2 = new Thread(() =>
            {
                Thread.Sleep(TimeSpan.FromSeconds(5));
                SharedVariable2 = "Two";

            });
            var t3 = new Thread(() =>
            {
                Thread.Sleep(TimeSpan.FromSeconds(5));
                SharedVariable3 = "Three";

            });
            t1.Start(); t2.Start(); t3.Start();
            t1.Join(); t2.Join(); t3.Join();
        }
    }
}
