﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._001_threads
{
    class _007_ThreadPriority
    {
        static void Main1(string[] args)
        {
            Thread t1 = new Thread(()=> DoSomeThing("Thread 1 "));
            Thread t2 = new Thread(() => DoSomeThing("Thread 2 "));
            t2.Priority = ThreadPriority.Highest;
            t1.Start(); t2.Start();
            t1.Join();
            t2.Join();
            Console.WriteLine("Completed ");
            Console.ReadLine();


        }

        private static void DoSomeThing(string p)
        {
            Console.WriteLine("Executing "+p);
        }
    }
}
