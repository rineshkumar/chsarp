﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _0045_ThreadWithCapturedVariables
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                var local = i;
                new Thread(() => { Console.WriteLine(local); }).Start();
            }
            Console.Read();
        }
    }
}
