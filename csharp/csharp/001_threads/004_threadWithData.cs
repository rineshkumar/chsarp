﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _004_threadWithData
    {
        static void Main1(string[] args)
        {
            Thread t1 = new Thread(() => FunctionWithParameters("Hello World"));
            t1.Start();
            t1.Join();
            Console.WriteLine("Execution completed");
            Console.ReadLine();
        }
        /*
         * An operation which is performing CPU intensive work is called CPU bound operation. 
         * AN operation which spends most of its time waiting , or is sleeping is called I/O bound
         * Operation works in Two ways 
         * 1. Waits synchronously on the thread . Ex. Console.ReadLine . They block the thread. They may also spin periodically 
         * while (DateTime.Now < nextStartTime)Thread.Sleep (100);
         * 2. Operates asynchronously , executing a callback when operation completes
         * 
         */
        private static void FunctionWithParameters(object obj)
        {
            Console.WriteLine("Information passed to the thread " + obj);
        }
    }
}
