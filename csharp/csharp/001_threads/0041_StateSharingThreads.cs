﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    /*
     *  Threads have their owm memory stack . 
     *  They dont share local variables. 
     *  There is a copy of i0 for each variable.
     */
    class _0041_StateSharingThreads
    {
        public void Go(string callingThread)
        {

            for (int i0 = 0; i0 < 10; i0++)
            {
                Thread.Sleep(TimeSpan.FromSeconds(1));
                System.Console.WriteLine(callingThread + ": Running go method " + i0);
            }
        }
        static void Main(string[] args)
        {
            _0041_StateSharingThreads tt = new _0041_StateSharingThreads();
            var t1 = new Thread(() => tt.Go("First Thread"));
            var t2 = new Thread(() => tt.Go("Second Thread"));
            t1.Start(); t2.Start();
            t1.Join();
            t2.Join();
            Console.Write("Done");
            Console.ReadLine();
        }
    }

}
