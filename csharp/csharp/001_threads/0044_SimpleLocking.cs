﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _0044_SimpleLocking
    {
        bool _showedMessage;
        private readonly object _objectForLocking = new object();
        public void ShowMessage()
        {
            lock (_objectForLocking)
            {
                if (!_showedMessage)
                {
                    System.Console.WriteLine("Showing Message ");
                    _showedMessage = true;
                }
            }

        }
        static void Main(string[] args)
        {
            _0044_SimpleLocking ss = new _0044_SimpleLocking();
            var t1 = new Thread(() => { ss.ShowMessage(); });
            t1.Start();
            ss.ShowMessage();
            Console.ReadLine();
        }
    }
}
