﻿using System;
using System.Threading;
/*
 * 1. Multiple processing  -- Multiple requests processed in parallel / Concurrently / Simultaneously
 * 2. Multithreading - Multiple threads running in the same process sharing the same execution environment (cpu, memory) 
 * 3. Threads may be working on same data (one readin, one writing). This data is called shared state
 */
namespace csharp._001_threads
{
    public class _001_ThreadSample
    {
        static void Main1(string[] args)
        {
            Thread t1 = new Thread(SomeFunction);
            t1.Start();
            t1.Join();
            Console.WriteLine("Thread Execution completed");
            Console.ReadLine();
        }

        private static void SomeFunction(object obj)
        {
            Console.WriteLine("Thread Executing ");
        }
    }
}
