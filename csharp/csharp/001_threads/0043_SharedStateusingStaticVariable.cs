﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _0043_SharedStateusingStaticVariable
    {
        static void Main(string[] args)
        {
            var t1 = new Thread(() => { ClassWithStaticData.ShowMessage(); });
            t1.Start();
            ClassWithStaticData.ShowMessage();
            Console.ReadLine();
        }

    }
    class ClassWithStaticData
    {
        static bool _messageDisplayed;
        public static void ShowMessage()
        {
            if (!_messageDisplayed)
            {
                System.Console.WriteLine("Message Displayed");
                _messageDisplayed = true;
            }
        }
    }
}
