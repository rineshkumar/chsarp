﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _0042_ThreadSharingInstanceVariable
    {
        /*
         * When an instance method is invoked by the thread and method uses instance variable, then the insatance variable will not be thread safe
         * Same is the case with Lambda expression which captures a variable 
         */

        static void Main(string[] args)
        {
            ClassForThread cc = new ClassForThread();
            var t1 = new Thread(cc.ShowMessage);
            t1.Start();
            // Thread.Sleep(TimeSpan.FromSeconds(1));
            cc.ShowMessage();
            t1.Join();
            Console.ReadLine();
        }
    }
    class ClassForThread
    {
        private bool _DisplayMessage;
        public void ShowMessage()
        {
            if (!_DisplayMessage)
            {

                System.Console.WriteLine("Displaying Message ");
                _DisplayMessage = true;
            }
        }
    }
}
