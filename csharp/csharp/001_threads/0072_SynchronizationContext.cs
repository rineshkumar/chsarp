﻿using System;
using System.Threading;

namespace csharp._001_threads
{
    class _0072_SynchronizationContext
    {

        SynchronizationContext sc;
        Thread WorkerThread;

        public _0072_SynchronizationContext()
        {
            this.sc = new SynchronizationContext();
        }

        static void Main(string[] args)
        {
            _0072_SynchronizationContext context = new _0072_SynchronizationContext();
            new Thread(() => { context.ProcessData(); }).Start();
            Console.Read();
        }
        void DisplayMessage(object data)
        {
            System.Console.WriteLine(data);
        }
        void ProcessData()
        {
            Thread.Sleep(TimeSpan.FromSeconds(5));
            Console.WriteLine("Heavy Duty Work completed.");
            this.sc.Post(DisplayMessage, "hello World");
        }
    }
}
