﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._001_threads
{
    class _008_usingThreadFromThreadPool
    {
        /*
        Threads from thread pool - background threads, cannot set their name.
        Useful for fine grained concurrency - Allows small asynchronous operations without being overwhelmed by the time consumed during thread start up. 
        
        Useful for parallem programming.
        Tips - Cpu oversubscription should not happen. - More active threads than the number of cores in the cpu. Will lead to context switching hurting overall performance.
        */
        static void Main(string[] args)
        {
            var t1 = Task.Run(() => { Console.WriteLine("Using thread from thread pool " + Thread.CurrentThread.IsThreadPoolThread); });
            t1.Wait();
            Console.WriteLine("COmpleted");
            Console.ReadLine();

        }
    }
}
