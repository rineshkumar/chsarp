﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._012_StreamAdapters
{
    class _001_StreamReaderAndWriter
    {
        static void Main1(string[] args)
        {
            WriteFile();
            ReadFile();
            Console.WriteLine("Done");
            Console.ReadLine();
        }
        private static void WriteFile()
        {
            using (StreamWriter sw = new StreamWriter(@"C:\Users\rinesh_kumar\Documents\sample.txt"))
            {
                sw.WriteLine("This is a line ");
            }
        }

        private static void ReadFile()
        {
            using (StreamReader sr = new StreamReader(@"C:\Users\rinesh_kumar\Documents\sample.txt"))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
            }
        }
    }
}
