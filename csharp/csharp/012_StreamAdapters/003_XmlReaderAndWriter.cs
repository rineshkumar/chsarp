﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
namespace csharp._012_StreamAdapters
{
    class _003_XmlReaderAndWriter
    {
        static void Main1(string[] args)
        {
            _003_EntitiyForXmlWriter entity = new _003_EntitiyForXmlWriter { id = 1, FirstName = "Rinesh", IsEmployed = false, BirthDate = DateTime.Now };
            writeToXml(entity);
            readFromXml(@"C:\Users\rinesh_kumar\Documents\Employees.xml");
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void readFromXml(string p)
        {
            using (XmlReader reader = XmlReader.Create(p))
            {
                _003_EntitiyForXmlWriter entity = new _003_EntitiyForXmlWriter();
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name) {
                            case "Employee":
                                entity = new _003_EntitiyForXmlWriter();
                                break;
                            case "ID":
                                if (reader.Read()) {
                                    int result;
                                    int.TryParse(reader.Value,out result);
                                    entity.id = result;
                                }
                                break;
                            case "FirstName":
                                if (reader.Read())
                                {
                                    entity.FirstName = reader.Value;
                                }
                                break;
                            case "IsEmployed":
                                if (reader.Read()) {
                                    bool result;
                                    bool.TryParse(reader.Value, out result);
                                    entity.IsEmployed = result;
                                }
                                break;
                            case "BirthDate":
                                if (reader.Read()) {
                                    DateTime result;
                                    DateTime.TryParse(reader.Value, out result);
                                    entity.BirthDate = result;
                                }
                                break;
                        }
                    }
                }
                Console.WriteLine("{0}-{1}-{2}-{3}",entity.id,entity.FirstName,entity.IsEmployed,entity.BirthDate);
            }
        }

        private static void writeToXml(_003_EntitiyForXmlWriter entity)
        {
            XmlWriterSettings setting = new XmlWriterSettings();
            setting.Indent = true;
            
            using (XmlWriter writer = XmlWriter.Create(@"C:\Users\rinesh_kumar\Documents\Employees.xml",setting))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Employee");
                writer.WriteElementString("ID", entity.id.ToString());
                writer.WriteElementString("FirstName", entity.FirstName);
                writer.WriteElementString("IsEmployed", entity.IsEmployed.ToString());
                writer.WriteElementString("BirthDate", entity.BirthDate.ToString());
            }
        }
    }
}
