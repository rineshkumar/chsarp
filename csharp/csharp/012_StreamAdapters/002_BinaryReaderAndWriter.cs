﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace csharp._012_StreamAdapters
{
    class _002_BinaryReaderAndWriter
    {
        static void Main1(string[] args)
        {
            ReadFromFile();
            WriteToFile();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void WriteToFile()
        {
            using (BinaryWriter bw = new BinaryWriter(new FileStream(@"C:\Users\rinesh_kumar\Documents\sampleBinary.binary",FileMode.Create)))
            {
                bw.Write(1.25F);
                bw.Write("Hello World");
                bw.Write(10);
                bw.Write(true);

            }
        }

        private static void ReadFromFile()
        {
            using (BinaryReader br = new BinaryReader(new FileStream(@"C:\Users\rinesh_kumar\Documents\sampleBinary.binary",FileMode.Open)))
            {
                Console.WriteLine(br.ReadSingle());
                Console.WriteLine(br.ReadString());
                Console.WriteLine(br.ReadInt32());
                Console.WriteLine(br.ReadBoolean());
            }
        }
    }
}
