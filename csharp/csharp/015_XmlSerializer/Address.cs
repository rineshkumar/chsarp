﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace csharp._015_XmlSerializer
{
    [XmlInclude(typeof(AddressSubclass))]
    public class Address
    {
        public string Street { get; set; }
    }
    public class AddressSubclass : Address
    {
        public string City { get; set; }
    }
}
