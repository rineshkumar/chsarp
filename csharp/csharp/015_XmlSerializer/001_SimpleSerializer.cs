﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace csharp._015_XmlSerializer
{

    [XmlInclude(typeof(EntityForXmmlSerializerSubClass))]
    public class EntityForXmlSerializer
    {

        public int Id { get; set; }
        [XmlElement(ElementName = "FirstName")]
        public string Name { get; set; }
        [XmlIgnore]
        public int IgnoreInt { get; set; }
        [XmlAttribute]
        public string AttributeProperty { get; set; }
        [XmlArray(ElementName = "ListOfNames")]
        [XmlArrayItem(ElementName = "Name")]
        public List<string> StringList { get; set; }
        [XmlElement("ExtendedAddress", typeof(AddressSubclass))]
        public Address HomeAddress { get; set; }
        [XmlArrayItem("BaseAddress",typeof(Address))]
        [XmlArrayItem("SubAddress", typeof(AddressSubclass))]
        public List<Address> Adresses { get; set; }
    }
    public class EntityForXmmlSerializerSubClass : EntityForXmlSerializer
    {

        public int SubClassProperty { get; set; }
    }
    class _001_SimpleSerializer
    {
        static void Main1(string[] args)
        {
            WriteObject();
            ReadObject();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(EntityForXmlSerializer));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\BS_SimpleXml.txt", FileMode.Open))
            {
                var entity = (EntityForXmmlSerializerSubClass)serializer.Deserialize(s);
                Console.WriteLine("{0}--{1}--{2}--{3}", entity.Id, entity.Name, entity.AttributeProperty, entity.StringList);
                foreach (var item in entity.StringList)
                {
                    Console.Write(item + " ");
                }
                Console.WriteLine(entity.SubClassProperty);
                Console.WriteLine(((AddressSubclass)entity.HomeAddress).City);
            }
        }

        private static void WriteObject()
        {
            //EntityForXmlSerializer entity = new EntityForXmlSerializer();
            EntityForXmmlSerializerSubClass entity = new EntityForXmmlSerializerSubClass();
            entity.Id = 10;
            entity.Name = "Name";
            entity.AttributeProperty = "AttributeValue";
            entity.StringList = new List<string> { "A", "B" };
            entity.SubClassProperty = 10;
            entity.HomeAddress = new AddressSubclass { Street = "SomeStreet", City = "City 1" };
            entity.Adresses = new List<Address>();
            entity.Adresses.Add(new Address { Street = "AddressStreet" });
            entity.Adresses.Add(new AddressSubclass { Street = "AddressStreet", City = "City 2" });
            XmlSerializer serializer = new XmlSerializer(typeof(EntityForXmlSerializer));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\BS_SimpleXml.txt", FileMode.Create))
            {
                serializer.Serialize(s, entity);
            }
        }
    }
}
