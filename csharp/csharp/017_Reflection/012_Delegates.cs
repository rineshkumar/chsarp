﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    public delegate int MyFunc(int a);
    public class MyProgram
    {
        public static int Square(int i) // should be static 
        {
            return i * i;
        }
        public static int Cube(int i) // should be static 
        {
            return i * i * i;
        }
    }
    public class _012_Delegates
    {
        static void Main1(string[] args)
        {
            Delegate d = Delegate.CreateDelegate(typeof(MyFunc), typeof(MyProgram), "Square");

            Console.WriteLine(d.DynamicInvoke(10));
            Console.ReadLine();
        }
    }
}
