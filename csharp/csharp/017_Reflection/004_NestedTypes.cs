﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _004_NestedTypes
    {
        static void Main1(string[] args)
        {
            var environmentSubStypes = typeof(System.Environment).GetNestedTypes();
            foreach (var type in environmentSubStypes)
            {
                Console.WriteLine(type.FullName);
            }
            var t = typeof(string);
            Console.WriteLine(t.Namespace);
            Console.WriteLine(t.Name);
            Console.WriteLine(t.FullName);
            Console.Read();
        }
    }
}
