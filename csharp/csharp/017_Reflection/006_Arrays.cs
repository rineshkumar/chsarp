﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _006_Arrays
    {
        static void Main1(string[] args)
        {
            var intArrayType = typeof(int[]);
            Console.WriteLine(intArrayType.FullName);
            var int2dArrayType = typeof(int[,]);
            Console.WriteLine(int2dArrayType.FullName);
            Console.ReadLine();
        }
    }
}
