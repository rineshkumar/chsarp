﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _010_Activator
    {
        static void Main1(string[] args)
        {
            var intInstance = Activator.CreateInstance(typeof(int));
            var dateInstance = Activator.CreateInstance(typeof(DateTime), 2017, 10, 10);
            Console.WriteLine(intInstance.GetType());
            Console.WriteLine(dateInstance.GetType());
            Console.ReadLine();
        }
    }
}
