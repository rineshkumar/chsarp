﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _016_methodParameterDetaisl
    {
        static void Main1(string[] args)
        {
            //Get the type 
            var stringType = typeof(string);
            //Get the meethodInfo
            var parameterType = new Type[] { typeof(int) };
            MethodInfo methodInfo = stringType.GetMethod("Substring", parameterType);
            var parameterInfo = methodInfo.GetParameters();
            foreach (var item in parameterInfo)
            {
                Console.WriteLine(item.Name+" "+item.ParameterType);
            }
            Console.Read();
        }
    }
}
