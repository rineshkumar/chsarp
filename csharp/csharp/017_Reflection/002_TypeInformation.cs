﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _002_TypeInformation
    {
        static void Main1(string[] args)
        {
            var strignType = typeof(String);
            Console.WriteLine("{0}-{1}-{2}-{3}",strignType.Name,strignType.BaseType,strignType.Assembly.FullName,strignType.IsPublic);
            Console.ReadLine();
        }
    }
}
