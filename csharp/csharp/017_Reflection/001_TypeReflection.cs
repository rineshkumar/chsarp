﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _001_TypeReflection
    {
        static void Main1(string[] args)
        {
            var compileTimeStringType = typeof(String);
            Console.WriteLine("compileTimeStringType " + compileTimeStringType);
            var compileTimeStringAType = typeof(String[]);
            Console.WriteLine("compileTimeStringAType " + compileTimeStringAType);
            var compileTimeString2DAType = typeof(String[,]);
            Console.WriteLine("compileTimeString2DAType " + compileTimeString2DAType);
            var openGenericDictionary = typeof(Dictionary<,>);
            Console.WriteLine("openGenericDictionary " + openGenericDictionary);
            var closedGenericDictionary = typeof(Dictionary<int, int>);
            Console.WriteLine("closedGenericDictionary " + closedGenericDictionary);
            var runningType = Assembly.GetExecutingAssembly().GetType("csharp._017_Reflection._001_TypeReflection");
            Console.WriteLine(runningType.FullName);
            Console.WriteLine("runningType " + runningType);
            Console.WriteLine(runningType.FullName);
            var typeFromFullName = Type.GetType("csharp._017_Reflection._001_TypeReflection");
            Console.WriteLine("typeFromFullName " + typeFromFullName);
            Console.ReadLine();
        }
    }
}
