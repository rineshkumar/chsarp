﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _005_GenericTypes
    {
        static void Main1(string[] args)
        {
            var openType = typeof(Dictionary<,>);
            Console.WriteLine(openType.Name);
            Console.WriteLine(openType.FullName);
            var closedType = typeof(Dictionary<int, int>);
            Console.WriteLine(closedType.FullName);
            Console.ReadLine();
        }
    }
}
