﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _011_ConstructorInfo
    {
        static void Main1(string[] args)
        {
            var stringType = typeof(String);
            var constructor = typeof(String).GetConstructor(new Type[] { typeof(Char[]) });
            var stringObject = constructor.Invoke(new Object[] { new char[]{'R','i','n','e','s','h'} });
            Console.WriteLine(stringObject);
            Console.ReadLine();
        }
    }
}
