﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _008_IsInstanceOfType
    {
        static void Main1(string[] args)
        {
            var guid = Guid.NewGuid();
            var isFormattable = guid is IFormattable;
            Console.WriteLine(isFormattable);// returns true 
            var interfaceType = typeof(IFormattable);
            var result = interfaceType.IsInstanceOfType(guid); // returns true 
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
