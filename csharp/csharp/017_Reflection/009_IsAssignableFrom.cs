﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _009_IsAssignableFrom
    {
        static void Main1(string[] args)
        {
            var stringType = typeof(string);
            var comparableType = typeof(IComparable);
            var result = comparableType.IsAssignableFrom(stringType);
            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}
