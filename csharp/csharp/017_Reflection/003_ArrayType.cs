﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _003_ArrayType
    {
        static void Main1(string[] args)
        {
            var intType = typeof(int).MakeArrayType();
            Console.WriteLine("intType == typeof(int[]) -->" +(intType == typeof(int[])));
            
            var TwoDIntType = typeof(int).MakeArrayType(2);
            Console.WriteLine("TwoDIntType == typeof(int[,]) --> " + (TwoDIntType == typeof(int[,])));
            

            var baseType = TwoDIntType.GetElementType();
            var dimension = TwoDIntType.GetArrayRank();
            Console.WriteLine("TwoDIntType.GetElementType() --> "+baseType);
            Console.WriteLine("TwoDIntType.GetArrayRank() --> "+ dimension);
            Console.ReadLine();
        }
    }
}
