﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _015_InvokingMember
    {
        static void Main1(string[] args)
        {
            //1.Get the type 
            var stringType = typeof(string);
            //get constructor info 
            var constructorInfo = stringType.GetConstructor(new Type[]{typeof(char[])});
            //create the object 
            var stringInstance = constructorInfo.Invoke(new object[] { new char[] { 'R', 'I', 'N', 'E', 'S', 'H' } });
            //get the parameter for the method 
            var parameterType = new Type[] { typeof(int) };
            //Get the method info 
            MethodInfo methodInfo = stringType.GetMethod("Substring", parameterType);
            //4.get argument to be used for calling method . 
            object[] parameterValues = new object[] { 3 };
            //5.call the method.
            var result = methodInfo.Invoke(stringInstance, parameterValues);
            Console.WriteLine(result);
            Console.ReadLine();

        }
    }
}
