﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _007_BaseTypeAndInterfa_e
    {
        static void Main1(string[] args)
        {
            var stringType = typeof(string);
            Console.WriteLine(stringType.BaseType.FullName);
            var interfaces = typeof(Guid).GetInterfaces();
            foreach (var item in interfaces)
            {
                Console.WriteLine(item.FullName);
            }
            Console.ReadLine();
        }
    }
}
