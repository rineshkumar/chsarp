﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _013_genericType
    {
        static void Main1(string[] args)
        {
            var unboundedType = typeof(List<>);
            //Creating closed type 
            var closedType = unboundedType.MakeGenericType(typeof(int));
            //Creating opentype
            var u2 = closedType.GetGenericTypeDefinition();
            Console.WriteLine(unboundedType.FullName);
            Console.WriteLine(closedType.FullName);
            Console.WriteLine(u2.FullName);
            var type = typeof(List<>);
            //Check if a type is generic type.
            Console.WriteLine(type.IsGenericType);
            var arguments = closedType.GenericTypeArguments;
            foreach (var item in arguments)
            {
                Console.WriteLine(item.FullName);
            }
            Console.ReadLine();
        }
    }
}
