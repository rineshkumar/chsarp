﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._017_Reflection
{
    class _014_GettingMembers
    {
        static void Main1(string[] args)
        {
            var stringType = typeof(string);
            var members = stringType.GetMembers();//Getting all the members 
            
            foreach (var item in members)
            {
                Console.WriteLine(item.Name+" "+item.ReflectedType+" "+item.DeclaringType);
            }
            var member = stringType.GetMember("ToString");//getting a single member
            Console.WriteLine(member[0].Name);
            Console.Read();
        }
    }
}
