﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace csharp._016_Assemblies
{
    class _005_AssemblyResolve
    {
        static void Main1(string[] args)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += currentDomain_AssemblyResolve;
            var handle = currentDomain.CreateInstance("RequiredAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "RequiredAssembly.DependentType");
            var obj = handle.Unwrap();
            Console.ReadLine();
        }

        private static System.Reflection.Assembly currentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            Console.WriteLine("Assembly not found");
            //Loading From file 
            var a = Assembly.LoadFrom(@"C:\Users\rinesh_kumar\Documents\RequiredAssembly\RequiredAssembly\bin\Debug\RequiredAssembly.EXE");
            return a;
        }
    }
}
