﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace csharp._016_Assemblies
{
    class _001_AssemblyClass
    {
        static void Main1(string[] args)
        {
            Assembly a = typeof(_001_ClassForAssemblyClass).Assembly;
            Console.WriteLine(Assembly.GetExecutingAssembly());
            Console.WriteLine(Assembly.GetCallingAssembly());
            Console.WriteLine(Assembly.GetEntryAssembly());


            Console.WriteLine("{0} - {1}  ",a.FullName,a.ImageRuntimeVersion);

            Console.ReadLine();
        }
    }
}
