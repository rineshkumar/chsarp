﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace csharp._016_Assemblies
{
    class _003_AssemblyLoadFile
    {
        static void Main1(string[] args)
        {
            var a = Assembly.LoadFile(@"C:\Users\rinesh_kumar\Documents\csharp\csharp\csharp\bin\Debug\csharp.EXE");
            var requiredType = a.GetType("csharp._016_Assemblies._003_TypeForLoadFrom");
            _003_TypeForLoadFrom type = (_003_TypeForLoadFrom)Activator.CreateInstance(requiredType);
            type.sayHello();
            Console.Read();
        }
    }
}
