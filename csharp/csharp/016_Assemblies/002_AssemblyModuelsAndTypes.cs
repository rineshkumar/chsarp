﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace csharp._016_Assemblies
{
    class _002_AssemblyModuelsAndTypes
    {
        static void Main1(string[] args)
        {

            Assembly a = typeof(_001_ClassForAssemblyClass).Assembly;
            Console.WriteLine(a.FullName);
            Console.WriteLine(a.CodeBase);
            Console.WriteLine(a.Location);
            Console.WriteLine("Referenced Assemblies------------");
            foreach (var item in a.GetReferencedAssemblies())
            {
                Console.WriteLine(item.FullName);
            }
            Console.WriteLine("Assemblies in appdomain++++++++++++++++");
            int i = 0;
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (i < 1)
                {
                    i++;
                    Console.WriteLine(assembly.FullName);
                    Console.WriteLine("***************Modules***************");
                    foreach (var module in assembly.Modules)
                    {
                        Console.WriteLine("*** "+module.Name);
                    }
                    Console.WriteLine("&&&&&&&&&&&&&Exported types &&&&&&&&&&&");
                    foreach (var type in assembly.ExportedTypes.Distinct())
                    {
                        Console.WriteLine("***"+type.Name);
                        Console.WriteLine("%%%%%%Members%%%%%%%%%%");
                        foreach (var member in type.GetMembers())
                        {
                            Console.WriteLine("-----------"+member.Name);
                        }
                    }
                }

            }
            Console.ReadLine();
        }
    }
}
