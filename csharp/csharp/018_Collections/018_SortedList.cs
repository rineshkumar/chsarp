﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _018_SortedList
    {
        static void Main1(string[] args)
        {
            SortedList<string, int> sl = new SortedList<string, int>();
            sl.Add("a", 1);
            sl.Add("c", 1);
            sl.Add("e", 1);
            sl.Add("d", 1);
            sl.Add("b", 1);
            foreach (var item in sl)
            {
                Console.WriteLine(item.Key+" "+item.Value);
            }
            Console.Read();
        }
    }
}
