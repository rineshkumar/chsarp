﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _002_CustomCollection : IEnumerable
    {
        private object[] objects;
        int freeIndex = 0;
        public _002_CustomCollection()
        {
            objects = new object[100];
        }
        public void Add(object item)
        {
            objects[freeIndex] = item;
            freeIndex++;
        }
        static void Main1(string[] args)
        {
            _002_CustomCollection collection = new _002_CustomCollection();
            for (int i = 0; i < 70; i++)
            {
                collection.Add(i);
            }

            foreach (var item in collection)
            {
                Console.WriteLine(item);
            }
            Console.Read();
        }

        public IEnumerator GetEnumerator()
        {
            foreach (var item in objects) 
            {
                if (item == null)
                    break;
                yield return item;
            }
        }
    }
}
