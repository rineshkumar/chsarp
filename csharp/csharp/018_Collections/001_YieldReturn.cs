﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _001_YieldReturn
    {
        static IEnumerable<int> YieldReturn()
        {
            yield return 1; // yield saves the function state
            yield return 2;
            yield return 3;
        }
        static void Main1(string[] args)
        {
            // Lets see how yield return works
            foreach (var i in YieldReturn())
            {
                Console.WriteLine(i);
            }
            Console.ReadLine();
        }
    }
}
