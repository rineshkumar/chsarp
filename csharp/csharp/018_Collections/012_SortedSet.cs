﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class SortedSet
    {
        static void Main1(string[] args)
        {
            SortedSet<string> set = new SortedSet<string>();
            set.Add("R");
            set.Add("I");
            set.Add("N");
            set.Add("E");
            set.Add("S");
            set.Add("H");
            set.Add("H");//No duplicates 

            foreach (var item in set)//Elements printed in sorted order 
            {
                Console.Write(item + " ");
            }
            Console.Read();
        }
    }
}
