﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _017_SortedDictionary
    {
        static void Main1(string[] args)
        {
            SortedDictionary<string, int> sd = new SortedDictionary<string, int>();
            sd.Add("a", 1);
            sd.Add("c", 2);
            sd.Add("b", 3);
            sd.Add("e", 4);
            sd.Add("d", 5);
            foreach (var item in sd)
            {
                Console.WriteLine(item.Key+" "+item.Value);
            }
            Console.Read();
        }
    }
}
