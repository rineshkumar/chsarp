﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _013_Dictionary
    {
        static void Main1(string[] args)
        {
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            dictionary.Add("a", 1);
            dictionary.Add("b", 2);
            dictionary.Add("c", 3);
            foreach (var item in dictionary.Keys)
            {
                Console.WriteLine(item + " " + dictionary[item]);
            }
            foreach (var item in dictionary)
            {
                Console.Write(item.Key + " " + item.Value);
            }
            Console.WriteLine();
            Console.WriteLine("dictionary.ContainsKey(\"a\")" + dictionary.ContainsKey("a"));
            int value;
            dictionary.TryGetValue("d", out value);
            Console.WriteLine("dictionary.TryGetValue(\"d\")" + value);
            Console.Read();
        }
    }
}
