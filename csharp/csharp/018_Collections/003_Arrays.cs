﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace csharp._018_Collections
{
    class _003_Arrays
    {
        static void Main1(string[] args) // To use Array class element should implement IEnumerable 
        {
            String[] s = new String[] { "R", "I", "N", "E", "S", "H" };
            Array.Sort(s);// sort
            PrintArray(s);
            Console.WriteLine(Array.BinarySearch(s, "H")); // used on a sotred array 
            Array.Reverse(s); // reverse
            PrintArray(s);
            string[] copy = new string[s.Length];
            Array.Copy(s,copy,s.Length); // copy 
            PrintArray(copy);
            
            Console.ReadLine();
        }

        private static void PrintArray(String[] s)
        {
            foreach (var item in s)
            {
                Console.Write(item+" ");
            }
            Console.WriteLine("");
        }
    }
}
