﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _008_QueueOfT
    {
        static void Main1(string[] args)
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue("R");
            queue.Enqueue("I");
            queue.Enqueue("N");
            printQueue(queue);
            Console.WriteLine(queue.Peek());
            queue.Dequeue();
            printQueue(queue);
            Console.Read();
        }

        private static void printQueue(Queue<string> queue)
        {
            foreach (var item in queue)
            {
                Console.Write(item);
            }
            Console.WriteLine("");
        }
    }
}
