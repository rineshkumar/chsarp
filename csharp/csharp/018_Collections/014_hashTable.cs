﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace csharp._018_Collections
{
    class _014_hashTable
    {
        static void Main1(string[] args)
        {
            Hashtable ht = new Hashtable();
            
            ht.Add("a", "1");
            ht.Add("b", "2");
            ht.Add("c", "3");
            foreach (DictionaryEntry item in ht)
            {
                Console.WriteLine(item.Key+" "+item.Value);
            }
            Console.WriteLine("ht.Contains(\"a\")" + ht.Contains("a"));
            Console.WriteLine("ht.ContainsKey(\"a\")" + ht.ContainsKey("a"));
            
            Console.ReadLine();
        }
    }
}
