﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _010_BitArray
    {
        static void Main1(string[] args)
        {
            byte[] b = new byte[] { 64 };
            BitArray ba = new BitArray(b);
            b = new byte[] { 128 };
            BitArray bb = new BitArray(b);

            PrintBitArray(ba);
            Console.WriteLine("");
            PrintBitArray(bb);
            Console.WriteLine("");
            BitArray bc = ba.And(bb);
            PrintBitArray(bc);
            BitArray bd = ba.Or(bb);
            PrintBitArray(bd);
            Console.ReadLine();
        }

        private static void PrintBitArray(BitArray bc)
        {
            for (int i = 0; i < bc.Count; i++)
            {
                Console.Write(bc[i] + " ");
            }
            Console.WriteLine("");
        }
    }
}
