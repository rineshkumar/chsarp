﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _005_ArrayList
    {
        static void Main1(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add("A");
            list.Add("B");
            list.Add("C");
            list.Add("D");
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
