﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _015_ListDIctionary
    {
        static void Main1(string[] args)
        {
            ListDictionary dicitonary = new ListDictionary();
            dicitonary.Add("a", "1");
            dicitonary.Add("b", "2");
            dicitonary.Add("c", "3");

            foreach (DictionaryEntry item in dicitonary)
            {
                Console.WriteLine(item.Key+" "+item.Value);
            }
            Console.WriteLine(dicitonary.Contains("A"));
            Console.WriteLine(dicitonary.Contains("a"));
            Console.WriteLine(dicitonary["a"]);
            
            Console.Read();
        }
    }
}
