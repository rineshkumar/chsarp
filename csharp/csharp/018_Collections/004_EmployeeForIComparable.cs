﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _004_EmployeeForIComparable : IComparable<_004_EmployeeForIComparable>
    {
        public int Salary { get; set; }
        public string Name { get; set; }


        public int CompareTo(_004_EmployeeForIComparable other)
        {
            if (this.Salary == other.Salary) {
                return this.Name.CompareTo(other.Name);
            }
            return this.Salary.CompareTo(other.Salary);
                
        }
        static void Main1(string[] args)
        {
            List<_004_EmployeeForIComparable> list = new List<_004_EmployeeForIComparable>();
            list.Add(new _004_EmployeeForIComparable { Name = "A", Salary = 10000 });
            list.Add(new _004_EmployeeForIComparable { Name = "B", Salary = 8000 });

            list.Sort();

            foreach (var item in list)
            {
                Console.WriteLine(item.Name+" "+item.Salary);
            }
            Console.Read();
        }
    }
}
