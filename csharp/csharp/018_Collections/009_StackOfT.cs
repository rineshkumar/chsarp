﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class StackOfT
    {
        static void Main1(string[] args)
        {
            Stack<string> stack = new Stack<string>();
            stack.Push("R");
            stack.Push("I");
            stack.Push("N");
            printStack(stack);
            Console.WriteLine(stack.Peek());
            stack.Pop();
            printStack(stack);
            Console.ReadLine();
        }

        private static void printStack(Stack<string> stack)
        {
            foreach (var item in stack)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
    }
}
