﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class HashSet
    {
        static void Main1(string[] args)
        {
            HashSet<string> set = new HashSet<string>();
            set.Add("R");
            set.Add("I");
            set.Add("N");
            set.Add("E");
            set.Add("S");
            set.Add("H");
            set.Add("H"); // duplicates not allowed  
            foreach (var item in set)
            {
                Console.WriteLine(item);
            }
            Console.Read();
        }
    }
}
