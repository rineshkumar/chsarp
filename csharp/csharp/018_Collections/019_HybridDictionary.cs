﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _019_HybridDictionary
    {
        static void Main1(string[] args)
        {
            HybridDictionary hd = new HybridDictionary();
            hd.Add("a", 1);
            hd.Add("c", 2);
            hd.Add("e", 3);
            hd.Add("d", 4);
            hd.Add("b", 5);
            foreach (DictionaryEntry item in hd)
            {
                Console.WriteLine(item.Key+" "+item.Value);
            }
            Console.Read();
        }
    }
}
