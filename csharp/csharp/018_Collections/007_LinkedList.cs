﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._018_Collections
{
    class _007_LinkedList
    {
        static void Main1(string[] args)
        {
            LinkedList<string> list = new LinkedList<string>();
            list.AddLast("cat");
            list.AddLast("dog");
            list.AddLast("cow");
            list.AddFirst("first");
            var element = list.Find("dog");
            list.AddAfter(element, "mouse");
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
