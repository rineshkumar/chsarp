﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace csharp._018_Collections
{
    class _016_OrderedDictionary
    {
        static void Main1(string[] args)
        {
            OrderedDictionary od = new OrderedDictionary();
            od.Add("b", 1);
            od.Add("a", 2);
            od.Add("e", 3);
            od.Add("d", 4);
            od.Add("c", 5);
            PrintDictionary(od);
            od.Insert(0, "z", 10);
            od["b"] = 50;
            PrintDictionary(od);
            Console.Read();
        }

        private static void PrintDictionary(OrderedDictionary od)
        {
            foreach (DictionaryEntry item in od)
            {
                Console.WriteLine(item.Key + " " + item.Value);
            }
            Console.WriteLine();
        }
    }
}
