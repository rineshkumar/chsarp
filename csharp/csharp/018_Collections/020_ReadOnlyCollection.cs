﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;
namespace csharp._018_Collections
{
    class _020_ReadOnlyCollection
    {
        static void Main1(string[] args)
        {
            List<string> alphabetList = new List<string>();
            alphabetList.Add("A");
            alphabetList.Add("B");
            alphabetList.Add("C");
            alphabetList.Add("C");
            PrintList(alphabetList);
            
            IReadOnlyCollection<string> outerList = new ReadOnlyCollection<string>(alphabetList);
            PrintList(outerList);
            alphabetList.Add("D");
            PrintList(outerList);
            Console.Read();
        }

        private static void PrintList(IReadOnlyCollection<string> outerList)
        {
            foreach (var item in outerList)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }
    }
}
