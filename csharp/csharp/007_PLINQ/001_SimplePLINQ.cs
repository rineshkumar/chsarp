﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp._007_PLINQ
{
    /*
     * Parallel Programming - Programming to leverage multiple cores
    */
    class _001_SimplePLINQ
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            var squares = numbers.AsParallel().Select(n => n * n);
            foreach (var item in squares)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("OUtput not in sequence ");
            Console.ReadLine();
        }
    }
}
