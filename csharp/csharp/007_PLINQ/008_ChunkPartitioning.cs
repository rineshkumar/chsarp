﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace csharp._007_PLINQ
{
    /*
     * use Partitioner.Create
     */
    class _008_ChunkPartitioning
    {
        static void Main1(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //using chunk patitioning 
            var parallelNumbers = Partitioner.Create(numbers, true).AsParallel().Select(x => x);
            foreach (var item in parallelNumbers)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
