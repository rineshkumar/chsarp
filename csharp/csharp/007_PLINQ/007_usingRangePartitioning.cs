﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp._007_PLINQ
{
    /*
     * To use range partitioning call toList or toArray on the input sequence
     */
    class _007_usingRangePartitioning
    {
        static void Main1(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var parallelNumbers = numbers.ToArray().AsParallel().Select(x => x * x);
            foreach (var item in parallelNumbers)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }


    }
}
