﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp._007_PLINQ
{
    class _005_settingDegreeOfParallelism
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var squareList = numbers.AsParallel().WithDegreeOfParallelism(6).Select(x => x * x);
            foreach (var item in squareList)
            {
                Console.WriteLine(item);
            }
            Console.Read();
        }
    }
}
