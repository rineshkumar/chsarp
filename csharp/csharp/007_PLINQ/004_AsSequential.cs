﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp._007_PLINQ
{
    /*
     * As sequential is used to prevent executing the query in parallel 
     */
    class _004_AsSequential
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var parallelResult = numbers.AsParallel().Select(x => x);
            foreach (var item in parallelResult)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("========================");
            var sequentialResult = parallelResult.AsSequential();
            foreach (var item in sequentialResult)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
