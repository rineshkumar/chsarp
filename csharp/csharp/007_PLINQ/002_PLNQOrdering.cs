﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp._007_PLINQ
{
    class _002_PLNQOrdering
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var squares = numbers.AsParallel().AsOrdered().Select(x => x * x);
            foreach (var item in squares)
            {
                Console.WriteLine(item);

            }
            Console.ReadLine();
        }
    }

}
