﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp._007_PLINQ
{
    /*
     * Use break top cancel plinq 
     */
    class _006_CancellingPlinq
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var squareQuery = numbers.AsParallel().Select(x => x * x);
            foreach (var item in squareQuery)
            {
                if (item == 100)
                    break; // use break in foreach to cancel plinq
                Console.Write(item + " ");
            }
            Console.ReadLine();
        }
    }
}
