﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._007_PLINQ
{
    /*
     * 1. Seed for each thread, 
     * 2. Local total for each thread 
     * 3. Total of all threads
     */
    class _009_ParallelAggregate
    {
        static void Main1(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var aggregate = numbers
                            .AsParallel()
                            .Aggregate(() => 0,// seed vaue 
                            (currentThreadTotal, n) => currentThreadTotal + n,//Per thread aggregation 
                                (totalOfAllThreads, perThreadTotal) => totalOfAllThreads + perThreadTotal,//Total of all threads
                                (Result) => Result
                            );
            Console.WriteLine(aggregate);
            Console.Read();
        }
    }
}
