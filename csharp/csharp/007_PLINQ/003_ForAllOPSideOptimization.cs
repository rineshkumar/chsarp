﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp._007_PLINQ
{
    class _003_ForAllOPSideOptimization
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //output side optimization using forall
            numbers.AsParallel().Select(x => x * x).ForAll(s => { Console.WriteLine(s); });
            Console.ReadLine();
        }
    }
}
