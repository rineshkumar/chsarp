﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._004_ThreadSynchronization_ExclusiveLocking
{

    class _002_Mutex
    {
        /*
         * Mutex spans multiple processes
         */
        private static Mutex mutex = new Mutex();
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                int temp = i;
                Task.Run(() =>
                {
                    accessSharedResource(temp);
                });
            }
            Console.ReadLine();
        }

        private static void accessSharedResource(int temp)
        {
            Console.WriteLine("Thread {0} requesting access to shared resource ", temp);
            mutex.WaitOne();
            Thread.Sleep(2000);
            Console.WriteLine("Thread {0} working on  shared resource ", temp);
            mutex.ReleaseMutex();
            Console.WriteLine("Thread {0} exiting  ", temp);
        }
    }
}
