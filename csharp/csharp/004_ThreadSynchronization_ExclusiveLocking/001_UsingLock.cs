﻿using System;
using System.Threading.Tasks;

namespace csharp._004_ThreadSynchronization_ExclusiveLocking
{
    /*
     Synchronization 
     -- Important when multiple threads are accessing the same data . 
     -- Three categories 
         
     -- Exclusive locking
        -- lock 
        -- Mutex
     -- Non Exclusive locking .
        -- Semaphore Slim 
        -- ReaderWriteLock Slim
     -- Signalling. 
        -- ManualResetEEvent
        -- AutoResetEvent
        -- CountDownEvent 
        -- BarrierClasses
         */

    /*
     * Properties of Object used for locking .
     * -- Should be reference type 
     * -- Should be visible to all threads 
     * -- Lock when updating a shared property 
     */
    class _001_UsingLock
    {
        private static readonly Object _lock = new object();
        public static int SharedResurce { get; set; }
        static void Main1(string[] args)
        {
            for (int i = 0; i < 10; i++)
            {
                int tempp = i;
                Task.Run(() =>
                {

                    UpdateSharedResource(tempp);
                });
            }
            Console.WriteLine("Main completed ");
            Console.Read();
        }

        private static void UpdateSharedResource(int i)
        {
            lock (_lock)
            {
                Console.WriteLine("Shared resorce updated by thread {0} ", i);
                SharedResurce++;
                Console.WriteLine("value is  " + SharedResurce);
            }
        }
    }
}
