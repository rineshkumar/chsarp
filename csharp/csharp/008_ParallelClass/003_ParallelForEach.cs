﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace csharp._008_ParallelClass
{
    class _003_ParallelForEach
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Parallel.ForEach(numbers, (n) => Console.WriteLine(n * n));
            Console.ReadLine();
        }
    }
}
