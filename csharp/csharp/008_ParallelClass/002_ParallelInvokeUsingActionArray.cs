﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace csharp._008_ParallelClass
{
    class _002_ParallelInvokeUsingActionArray
    {
        /*
         * Invoke takes Action[] as input . 
         * Action is a delegate which does not return any output . 
         * If we are using any data structure for storing data from various threads, that data structure should be thread safe Ex. Concurrent collections.
         */
        static void Main(string[] args)
        {
            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            List<Action> functionList = new List<Action>();
            foreach (int item in numbers)
            {
                int temp = item;
                //action expression will not take any input 
                functionList.Add(() => { Console.WriteLine(temp); });
            }
            Parallel.Invoke(functionList.ToArray());
            Console.Read();
        }
    }
}
