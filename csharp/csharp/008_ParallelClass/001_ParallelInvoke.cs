﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._008_ParallelClass
{
    /*
     * 1. All static methods will block execution till all Task execution not completed.  
     * 2.Not optimized for I/O bound work 
     * 3. Here in case of millions of operations (represented by delegates) , they are divided into batches and allocated to tasks. 
     * 4. Parallel methods do partitioning by themselves, but collation needs to be done by us.
     * 5. Result should be stores in thread safe collection. Ex. ConcurrentBag / ConcurrentDictionary
     */
    class _001_ParallelInvoke
    {
        static void Main(string[] args)
        {
            //Taking an [] of Actions
            Parallel.Invoke(
                () =>
                {

                    Thread.Sleep(5000);
                    Console.WriteLine("Completed first task ");
                },
                () =>
                {
                    Thread.Sleep(3000);
                    Console.WriteLine("Completed second task");
                }
                );
            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}
