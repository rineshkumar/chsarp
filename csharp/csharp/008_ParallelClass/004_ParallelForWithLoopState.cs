﻿using System;
using System.Threading.Tasks;

namespace csharp._008_ParallelClass
{
    class _004_ParallelForWithLoopState
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Parallel.For(0, numbers.Length, (n, loopState) =>
            {
                Console.WriteLine(n * n);
                if (n == 5)
                {
                    Console.WriteLine("Breaking out of the loop ");
                    loopState.Break();
                }
            });
            Console.ReadLine();
        }
    }
}
