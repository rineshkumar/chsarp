﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace csharp._008_ParallelClass
{

    class ParallelForEachWithLoopState
    {
        static void Main(string[] args)
        {

            List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Parallel.ForEach(numbers, (n, loopState) =>
            {
                Console.WriteLine(n * n);
                if (n == 5)
                {
                    Console.WriteLine("Breaking out of the loop ");
                    loopState.Break();
                }
            });
            Console.Read();
        }
    }
}
