﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace csharp._006_MultiThreadedTimers
{
    class _002_MTTWrapper
    {
        static void Main1(string[] args)
        {
            Timer t = new Timer();
            t.Interval = 2000;
            t.Elapsed += CallMePereodically;
            t.Start();
            Console.Read();

        }

        private static void CallMePereodically(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("In pereodic method");
        }
    }
}
