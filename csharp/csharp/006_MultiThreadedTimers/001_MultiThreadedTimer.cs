﻿using System;
using System.Threading;

namespace csharp._006_MultiThreadedTimers
{
    /*
     * 1. Use threads from thread pool 
     * 2. The call back method will be using different threads every time
     * 3. Call back method will start irrespective of whether previous call back has completed or not. ==> they are not thread safe 
     */
    class _001_MultiThreadedTimer
    {
        static void Main(string[] args)
        {
            Timer t = new Timer(CallMePereodically // callback may execute on a different thread. 
                                , "Hello World "
                                , TimeSpan.FromMilliseconds(0) // Time after which method will be called
                                , TimeSpan.FromMilliseconds(2000)); // Pereodic intervel after which method wil be called 
            Console.ReadLine();
        }

        private static void CallMePereodically(object state)
        {
            Console.WriteLine(state);
        }
    }
}
