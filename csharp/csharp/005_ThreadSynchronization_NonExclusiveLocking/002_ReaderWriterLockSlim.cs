﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._005_ThreadSynchronization_NonExclusiveLocking
{
    class _002_ReaderWriterLockSlim
    {
        private static ReaderWriterLockSlim rwlock = new ReaderWriterLockSlim();
        static void Main(string[] args)
        {
            for (int i = 0; i < 6; i++)
            {
                int temp = i;
                Task.Run(() => { workOnSharedResource(temp); });
            }
            Console.Read();
        }

        private static void workOnSharedResource(int temp)
        {
            Console.WriteLine("Thread {0} requesting for read ", temp);
            rwlock.EnterReadLock();
            Console.WriteLine("Thread {0} reading ", temp);
            Thread.Sleep(2000);
            Console.WriteLine("Thread {0} exiting reading ", temp);
            rwlock.ExitReadLock();
            Console.WriteLine("Thread {0} requesting for write   ", temp);
            rwlock.EnterWriteLock();
            Console.WriteLine("Thread {0} writing....   ", temp);
            Thread.Sleep(2000);
            Console.WriteLine("Thread {0} exiting writing ", temp);
            rwlock.ExitWriteLock();
        }
    }
}
