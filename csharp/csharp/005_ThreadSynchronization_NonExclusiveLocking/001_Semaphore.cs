﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._005_ThreadSynchronization_NonExclusiveLocking
{
    class _001_Semaphore
    {
        public int SharedResource { get; set; }
        private static SemaphoreSlim semaphore = new SemaphoreSlim(3);
        static void Main(string[] args)
        {
            for (int i = 0; i < 6; i++)
            {
                int temp = i;
                Task.Run(() =>
                {
                    updateSharedResource(temp);
                });
            }
            Console.ReadLine();
        }

        private static void updateSharedResource(int temp)
        {
            Console.WriteLine("Thread {0} trying to enter shared area ", temp);
            semaphore.Wait();
            Console.WriteLine("Thread {0} working on shared area ", temp);
            Thread.Sleep(2000);
            semaphore.Release();
            Console.WriteLine("Thread {0} exiting shared area ", temp);
        }
    }
}
