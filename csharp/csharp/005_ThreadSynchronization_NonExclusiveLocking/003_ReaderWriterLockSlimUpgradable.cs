﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._005_ThreadSynchronization_NonExclusiveLocking
{
    class _003_ReaderWriterLockSlimUpgradable
    {
        private static ReaderWriterLockSlim rwLock = new ReaderWriterLockSlim();
        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++)
            {
                int temp = i;
                Task.Run(() =>
                {

                    WorkOnSharesResouce(temp);
                });
            }
            Console.ReadLine();
        }

        private static void WorkOnSharesResouce(int temp)
        {
            Console.WriteLine("Thread {0} trying to enter read area ", temp);
            rwLock.EnterUpgradeableReadLock();
            Console.WriteLine("Thread {0} reading ", temp);
            Thread.Sleep(2000);
            if (temp % 2 == 0)
            {
                Console.WriteLine("Thread {0} trying to enter write area ", temp);
                rwLock.EnterWriteLock();
                Console.WriteLine("Thread {0} updating resource ", temp);
                Thread.Sleep(2000);
                Console.WriteLine("Thread {0} exiting write area ", temp);
                rwLock.ExitWriteLock();

            }
            rwLock.ExitUpgradeableReadLock();
            Console.WriteLine("Thread {0} exiting read area ", temp);

        }
    }
}
