﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
namespace csharp._009_ConcurrentCollections
{
    class _005_BlockingCollection
    {
        static void Main(string[] args)
        {
            BlockingCollection<int> data = new BlockingCollection<int>();
            Task.Run(() =>
            {
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine("Adding item {0} to collection ", i);
                    data.Add(i);
                    Thread.Sleep(2000);
                }
            });
            Task.Run(() =>
            {
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine("Consuming element {0} from collection ", data.Take());
                }
            });
            Console.ReadLine();
        }
    }
}
