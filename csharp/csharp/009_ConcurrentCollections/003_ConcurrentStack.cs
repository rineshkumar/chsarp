﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
namespace csharp._009_ConcurrentCollections
{
    class _003_ConcurrentStack
    {
        static void Main1(string[] args)
        {
            ConcurrentStack<int> stack = new ConcurrentStack<int>();
            for (int i = 0; i < 10; i++)
            {
                int temp = i;
                Task.Run(() =>
                {
                    Console.WriteLine("Adding element {0} to stack ",temp);
                    stack.Push(temp);
                });
            }
            for (int i = 0; i < 10; i++)
            {
             
                Task.Run(() =>
                {
                    int element;
                    stack.TryPop(out element);
                    Console.WriteLine("Removing element {0} from stack ", element);
                });
            }
            Console.ReadLine();
        }
    }
}
