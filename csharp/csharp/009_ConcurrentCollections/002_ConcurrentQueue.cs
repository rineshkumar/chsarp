﻿using System;
using System.Collections.Concurrent;
using System.Threading;
namespace csharp._009_ConcurrentCollections
{
    class _002_ConcurrentQueue
    {
        static ConcurrentQueue<int> intQueue = new ConcurrentQueue<int>();
        static void Main(string[] args)
        {
            Timer inputDataTimer = new Timer(AddToCollection, null, 1000, 5000); // Slow insertion 
            Timer outputDataTimer = new Timer(ReadFromCollection, null, 1000, 2000); // Aggressive reading 
            Console.ReadLine();
        }

        private static void ReadFromCollection(object state)
        {
            int result;
            if (intQueue.TryDequeue(out result))
            {
                Console.WriteLine("-- Reading Element - Element found {0}", result);
            }
            else
            {
                Console.WriteLine("-- Reading Element - No data found to read ");
            }
        }

        private static void AddToCollection(object state)
        {
            Console.WriteLine("++ Adding element");
            intQueue.Enqueue(new Random().Next(0, 5));
        }
    }
}
