﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace csharp._009_ConcurrentCollections
{
    class _001_ConcurrentBag
    {
        static ConcurrentBag<int> bagOfInts = new ConcurrentBag<int>();
        static void Main(string[] args)
        {

            Timer inputDataTimer = new Timer(AddToCollection, null, 1000, 1000); // Fast insertion 
            Timer outputDataTimer = new Timer(ReadFromCollection, null, 1000, 5000); // Slow reading 
            Console.ReadLine();
        }

        private static void ReadFromCollection(object state)
        {
            int result;

            while (bagOfInts.TryTake(out result))
            {
                Console.WriteLine("Data found " + result);
            }
        }

        private static void AddToCollection(object state)
        {
            Console.WriteLine("Adding data ");
            bagOfInts.Add(new Random().Next(0, 5));
        }
    }
}
