﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace csharp._009_ConcurrentCollections
{

    class _004_ConcurrentDictionary
    {
        static ConcurrentDictionary<int, int> numberDictionary = new ConcurrentDictionary<int, int>();
        static int key = 0;


        static void Main(string[] args)
        {

            Timer inputDataTimer = new Timer(AddToCollection, key, 1000, 5000); // Slow insertion 
            Timer outputDataTimer = new Timer(ReadFromCollection, key, 1000, 2000); // Aggressive reading 
            Console.ReadLine();

        }
        private static void ReadFromCollection(object state)
        {
            int result;
            if (numberDictionary.TryRemove(key, out result))
            {
                Console.WriteLine("-- Reading Element with key {0} - Element found {1}", key, result);
            }
            else
            {
                Console.WriteLine("-- Reading Element with Key {0} - No data found to read ", key);
            }
        }

        private static void AddToCollection(object state)
        {

            var temp = new Random().Next(1, 5);
            Console.WriteLine("++ Adding element    " + temp);
            numberDictionary.TryAdd(key, temp * temp);
            //  key++;
        }

    }
}
