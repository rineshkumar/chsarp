﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace csharp._013_DataContractSerializer
{
    class _010_DCS_CollectionSubClass
    {
        static void Main1(string[] args)
        {
            WriteObject();
            ReadObject();
            Console.WriteLine("Done ");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(_010_CustomCollection));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_CollectionSubClass.txt", FileMode.Open)) 
            {
              var   entity = (_010_CustomCollection)dcs.ReadObject(s);
            }
        }

        private static void WriteObject()
        {
            _010_CustomCollection c = new _010_CustomCollection();
            c.Add("a");
            c.Add("b");
            DataContractSerializer dcs = new DataContractSerializer(typeof(_010_CustomCollection));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_CollectionSubClass.txt", FileMode.Create))
            {
                dcs.WriteObject(s, c);
            }
        }
    }
}
