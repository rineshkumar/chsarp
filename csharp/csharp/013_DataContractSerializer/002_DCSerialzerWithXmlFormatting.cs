﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
namespace csharp._013_DataContractSerializer
{
    class _002_DCSerialzerWithXmlFormatting
    {
        static void Main1(string[] args)
        {
            WriteObject();
            ReadObject();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
           // throw new NotImplementedException();
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCSPersonFormatted.xml", FileMode.Open)) 
            {
                var entity  = (_001_EntityForDCSerializer)dcs.ReadObject(s);
                Console.WriteLine("{0} -- {1} -- {2} ",entity.Name,entity.DateOfBirth,entity.IsEmployed);
            }
        }

        private static void WriteObject()
        {
            _001_EntityForDCSerializer entity = new _001_EntityForDCSerializer();
            entity.Name = "test";
            entity.IsEmployed = true;
            entity.DateOfBirth = DateTime.Now;
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            using (XmlWriter writer  = XmlWriter.Create(@"C:\Users\rinesh_kumar\Documents\DCSPersonFormatted.xml",settings))
            {
                dcs.WriteObject(writer, entity);
            }
        }
    }
}
