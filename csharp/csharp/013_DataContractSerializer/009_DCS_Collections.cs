﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace csharp._013_DataContractSerializer
{
    [DataContract]
    class EntityWithCollection
    {
        [DataMember]
        public List<string> _member;
        public List<string> Members
        {
            get { return _member; }
            set { _member = value; }
        }
    }
    class _009_DCS_Collections
    {
        static void Main1(string[] args)
        {

            WriteObject();
            ReadObject();
            Console.WriteLine("Done");
            Console.ReadLine();

        }

        private static void ReadObject()
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(EntityWithCollection));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_Collection.txt", FileMode.Open))
            {
                var entity = (EntityWithCollection)dcs.ReadObject(s);
                Console.WriteLine(entity.Members.Count);
            }
        }

        private static void WriteObject()
        {
            EntityWithCollection ewc = new EntityWithCollection();
            ewc.Members = new List<string> { "a", "b", "c" };
            DataContractSerializer dcs = new DataContractSerializer(typeof(EntityWithCollection));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_Collection.txt", FileMode.Create))
            {
                dcs.WriteObject(s, ewc);
            }
        }
    }
}
