﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace csharp._013_DataContractSerializer
{
    [DataContract]
    public class SimpleEntity 
    { 
        [DataMember]
        public string Name { get; set; }
        [OnSerializing]
        public void BeforeSerializing(StreamingContext sc)
        {
            Console.WriteLine("Before Serializing");
        }
        [OnSerialized]
        public void AfterSerializing( StreamingContext sc)
        {
            Console.WriteLine("After Serializing");
        }
        [OnDeserializing]
        public void BeforeDeserializing(StreamingContext sc)
        {
            Console.WriteLine("Before DeSerializing");
        }
        [OnDeserialized]
        public void AfterDeserializing(StreamingContext sc)
        {
            Console.WriteLine("After DeSerializing");
        }
    }
    class _012_DCS_Events
    {
        static void Main1(string[] args)
        {
            WriteObject();
            ReadObject();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(SimpleEntity));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_Events.txt", FileMode.Open))
            {
                var e = dcs.ReadObject(s);
            }
        }

        private static void WriteObject()
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(SimpleEntity));
            SimpleEntity se = new SimpleEntity { Name = "Sample" };
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_Events.txt", FileMode.Create))
            {
                dcs.WriteObject(s, se);
            }
        }
    }
}
