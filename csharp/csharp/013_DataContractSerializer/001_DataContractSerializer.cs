﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace csharp._013_DataContractSerializer
{
    class _001_DataContractSerializer
    {
        static void Main1(string[] args)
        {

            SerializeObject();
            DeSerializerObject();
            Console.WriteLine("Done");
            Console.ReadLine();

        }

        private static void DeSerializerObject()
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCSPerson.xml", FileMode.Open)){
                _001_EntityForDCSerializer entity  =  (_001_EntityForDCSerializer)dcs.ReadObject(s);
                Console.Write("{0} -- {1} -- {2}", entity.Name, entity.IsEmployed, entity.DateOfBirth);
            }
        }

        private static void SerializeObject()
        {
            _001_EntityForDCSerializer entity = new _001_EntityForDCSerializer();
            entity.Name = "test";
            entity.IsEmployed = false;
            entity.DateOfBirth = DateTime.Now;
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCSPerson.xml", FileMode.Create))
            {
                dcs.WriteObject(s, entity);
            }
        }
    }
}
