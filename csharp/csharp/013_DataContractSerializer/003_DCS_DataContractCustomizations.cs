﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace csharp._013_DataContractSerializer
{
    class _003_DCS_DataContractCustomizations
    {
        static void Main1(string[] args)
        {
            WriteObject();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void WriteObject()
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            _001_EntityForDCSerializer entity = new _001_EntityForDCSerializer();
            entity.Name = "Test";
            entity.DateOfBirth = DateTime.Now;
            entity.IsEmployed = false;
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCSPersonCustomDataContract.xml", FileMode.Create))
            {
                dcs.WriteObject(s, entity);
            }
        }
    }
}
