﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace csharp._013_DataContractSerializer
{
    class _005_DCS_Subclass
    {
        static void Main1(string[] args)
        {
            WriteObject();
            Console.WriteLine("Done ");
            Console.ReadLine();
        }

        private static void WriteObject()
        {
            _001_Student s = new _001_Student();
            s.Name = "Test";
            s.DateOfBirth = DateTime.Now;
            s.IsEmployed = false;
            s.RollNumber = 1;

            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            using (Stream stream = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCSSubClass.txt", FileMode.Create))
            {
                dcs.WriteObject(stream, s);
            }
        }
    }
}
