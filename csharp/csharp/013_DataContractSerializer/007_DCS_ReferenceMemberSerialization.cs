﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace csharp._013_DataContractSerializer
{
    class _007_DCS_ReferenceMemberSerialization
    {
        static void Main1(string[] args)
        {
            WriteObject();
            ReadObject();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_ReferenceMemberSerialization.txt", FileMode.Open))
            {
                DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
                var entity = (_001_EntityForDCSerializer)dcs.ReadObject(s);
                Console.WriteLine("{0}-{1}-{2}",entity.Name,entity.DateOfBirth,entity.address.City);
            }
        }

        private static void WriteObject()
        {
            _001_EntityForDCSerializer entity = new _001_EntityForDCSerializer();
            entity.Name = "test";
            entity.DateOfBirth = DateTime.Now;
            entity.IsEmployed = true;
            entity.address = new _001_Address() { City = "Bangalore" };
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_ReferenceMemberSerialization.txt",FileMode.Create))
            {
                dcs.WriteObject(s, entity);    
            }
        }
    }
}
