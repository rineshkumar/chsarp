﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace csharp._013_DataContractSerializer
{
    [DataContract]
    class _001_Student : _001_EntityForDCSerializer
    {
        [DataMember]
        public int RollNumber { get; set; }
    }
}
