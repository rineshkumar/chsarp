﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace csharp._013_DataContractSerializer
{
    [DataContract(Name="Person"
        ,Namespace="www.google.com")
        , KnownType(typeof(_001_Student))]    
    class _001_EntityForDCSerializer
    {
        [DataMember(Name="PersonName",Order=1)]
        public String Name { get; set; }
        [DataMember(Name="BirthDay",Order=3)]
        public DateTime DateOfBirth { get; set; }
        [DataMember(Name="Working",Order=2)]
        public bool IsEmployed { get; set; }
        [DataMember(Name="PersonAddress",Order=4)]
        public _001_Address address { get; set; }
        [DataMember(Name = "PersonAddress2", Order = 5,EmitDefaultValue=false)]
        public _001_Address address2 { get; set; }
    }
}
