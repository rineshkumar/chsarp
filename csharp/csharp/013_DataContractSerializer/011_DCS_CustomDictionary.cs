﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace csharp._013_DataContractSerializer
{
    [CollectionDataContract(ItemName = "Addresses", KeyName = "Address", ValueName = "Home")]
    public class CustomDictionary : Dictionary<string, string> { }
    class _011_DCS_CustomDictionary
    {
        static void Main1(string[] args)
        {
            WriteObject();
            //ReadObject();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
            throw new NotImplementedException();
        }

        private static void WriteObject()
        {
            CustomDictionary cd = new CustomDictionary();
            cd.Add("a", "a");
            cd.Add("b", "b");
            DataContractSerializer dcs = new DataContractSerializer(typeof(CustomDictionary));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_CustomDictonary.txt", FileMode.Create))
            {
                dcs.WriteObject(s, cd);
            }
        }
    }
}
