﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace csharp._013_DataContractSerializer
{
    class _008_DCS_PreservingReference
    {
        static void Main1(string[] args)
        {
            WriteObject();
            ReadObject();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_PreservingReferece.txt", FileMode.Open))
            {
                var entity = (_001_EntityForDCSerializer)dcs.ReadObject(s);
                Console.WriteLine("{0} -- {1} ",entity.address.City,entity.address2.City);
            }
        }

        private static void WriteObject()
        {
            _001_EntityForDCSerializer entity = new _001_EntityForDCSerializer
            {
                Name = "test",
                DateOfBirth = DateTime.Now,
                IsEmployed = true,
                address = new _001_Address
                {
                    City = "Bangalore 1 "
                },
                
            };
            entity.address2 = entity.address;
            //DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            //In the xml z:Id and z:Ref are added
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer),null,1000,false,true,null);
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCS_PreservingReferece.txt", FileMode.Create))
            {
                dcs.WriteObject(s, entity);
            }
        }
    }
}
