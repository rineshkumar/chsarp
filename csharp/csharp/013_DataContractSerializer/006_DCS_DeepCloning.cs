﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
namespace csharp._013_DataContractSerializer
{
    class _006_DCS_DeepCloning
    {
        static void Main1(string[] args)
        {
            CloneData();
            Console.WriteLine("Done ");
            Console.ReadLine();
        }

        private static void CloneData()
        {
            _001_EntityForDCSerializer entity = new _001_EntityForDCSerializer();
            entity.Name = "Test";
            entity.DateOfBirth = DateTime.Now;
            entity.IsEmployed = false;
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            using (MemoryStream ms = new MemoryStream())
            {
                dcs.WriteObject(ms, entity);
                ms.Position = 0;
                _001_EntityForDCSerializer copy = (_001_EntityForDCSerializer)dcs.ReadObject(ms);
                Console.WriteLine("{0}--{1}--{2}",copy.Name,copy.DateOfBirth,copy.IsEmployed);
            }
        }
    }
}
