﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace csharp._013_DataContractSerializer
{
    [DataContract]
    class _001_Address
    {
        [DataMember]
        public string City { get; set; }
    }
}
