﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
namespace csharp._013_DataContractSerializer
{
    class _004_DcsAndBinaryFormatter
    {
        static void Main1(string[] args)
        {
            WriteObject();
            ReadObject();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            _001_EntityForDCSerializer entity;
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCSPersonBinary.txt", FileMode.Open))
            {
                using (XmlDictionaryReader reader = XmlDictionaryReader.CreateBinaryReader(s,XmlDictionaryReaderQuotas.Max))
                {
                    entity = (_001_EntityForDCSerializer)dcs.ReadObject(reader);
                    Console.WriteLine("{0} -- {1} -- {2} -- ",entity.Name,entity.DateOfBirth,entity.IsEmployed);
                }
            }
        }

        private static void WriteObject()
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(_001_EntityForDCSerializer));
            _001_EntityForDCSerializer entity = new _001_EntityForDCSerializer();
            entity.Name = "FirstName";
            entity.DateOfBirth = DateTime.Now;
            entity.IsEmployed = false;
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\DCSPersonBinary.txt", FileMode.Create))
            {
                using (XmlDictionaryWriter w = XmlDictionaryWriter.CreateBinaryWriter(s))
                {
                    dcs.WriteObject(w, entity);
                }
            }

        }
    }
}
