﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
namespace csharp._020_ConfigFile
{
    class _001_ConfigFileReading
    {
        public static void Main1(string[] args)
        {
            //reading all app setting 
            var appSettings = ConfigurationSettings.AppSettings;
            foreach (var item in appSettings.AllKeys)
            {
                Console.WriteLine(item+" " +appSettings[item]);
            }
            //raeding specific appsetting . 
            var result = appSettings["Setting1"];
            Console.WriteLine(result);
            //Reading connection strign 
            Console.WriteLine(ConfigurationManager.ConnectionStrings["WingtipToys"].ConnectionString);
            
            Console.ReadLine();
        }
    }

}
