﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace csharp._010_StreamsAndIO
{
    class _002_MemoryStream
    {
        static void Main1(string[] args)
        {
            Console.WriteLine("Writing to memory ");
            using (Stream stream= new MemoryStream())
            {
                string message = "Hello World ";
                byte[] b = Encoding.Unicode.GetBytes(message);
                stream.Write(b,0,b.Length);
                stream.Flush();

                Console.WriteLine("Reading from memory ");
                stream.Position = 0 ;
               int numberOfBytes = (int)stream.Length;
                int bytesReadPerInteration = 0;
                int totalBytesRead = 0;
                b = new byte[numberOfBytes];
                  while ((bytesReadPerInteration=stream.Read(b,totalBytesRead,numberOfBytes-bytesReadPerInteration))>0)
                {
                    totalBytesRead += bytesReadPerInteration;
                }
                String result = Encoding.Unicode.GetString(b);
                Console.WriteLine(result);
            }
            Console.ReadLine();
        }
    }
}
