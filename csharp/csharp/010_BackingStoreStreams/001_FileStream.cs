﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._010_StreamsAndIO
{
    class _001_FileStream
    {
        static void Main1(string[] args)
        {
            string s = "Hello World";
            Console.WriteLine("Writing to file ");
            using (Stream stream = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt", FileMode.Create))
            {
                byte[] b = Encoding.Unicode.GetBytes(s);
                stream.Write(b, 0, b.Length);
            }
            Console.WriteLine("Reading from file ");

            using (Stream stream = new FileStream(@"C:\Users\rinesh_kumar\Documents\sample.txt", FileMode.Open))
            {
                int numberOfBytes = (int)stream.Length;
                int bytesReadPerInteration = 0;
                int totalBytesRead = 0;
                byte[] b = new byte[numberOfBytes];
                while ((bytesReadPerInteration=stream.Read(b,totalBytesRead,numberOfBytes-bytesReadPerInteration))>0)
                {
                    totalBytesRead += bytesReadPerInteration;
                }
                String result = Encoding.Unicode.GetString(b);
                Console.WriteLine(result);
            }
            Console.Read();
        }
    }
}
