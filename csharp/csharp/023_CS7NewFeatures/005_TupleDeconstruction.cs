﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._023_CS7NewFeatures
{
    public class _005_TupleDeconstruction
    {
        public static void Main(string[] args)
        {
            _004_Tupes tupes = new _004_Tupes();
            (string firstName, string lastName) = tupes.ReturnATuple("Rinesh","Kumar");
            Console.WriteLine($"{firstName} -- {lastName}");
            Console.ReadLine();
        }
    }
}
