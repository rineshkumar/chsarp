﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;
namespace csharp._023_CS7NewFeatures
{
    public class _002_BinaryLiteral
    {
        static void Main(string[] args)
        {
            //Represent 50 in decimal, hexadecimal & binary  
            int a = 50; // decimal representation of 50  
            int b = 0X32; // hexadecimal representation of 50  
            int c = 0B110010; //binary representation of 50  
            //Represent 100 in decimal, hexadecimal & binary  
            int d = 50 * 2; // decimal represenation of 100   
            int e = 0x32 * 2; // hexadecimal represenation of 100  
            int f = 0b110010 * 2; //binary represenation of 100  
            WriteLine($"a: {a:0000} b: {b:0000} c: {c:0000}");
            WriteLine($"d: {d:0000} e: {e:0000} f: {f:0000}");
            WriteLine(f);// Using binary variables. 
            Console.ReadLine();
        }
    }
}
