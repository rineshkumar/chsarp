﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._023_CS7NewFeatures
{
    public class _004_Tupes
    {
        public (string, string) ReturnATuple(string firstName,string lastName)
        {
            return (firstName, lastName);
        }
        public static void Main(string[] args)
        {
            _004_Tupes tupleClass = new _004_Tupes();
            var returnTuple = tupleClass.ReturnATuple("Hello", " World");
            Console.WriteLine($"{returnTuple.Item1} {returnTuple.Item2}");
            Console.ReadLine();
        }
    }
}
