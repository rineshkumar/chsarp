﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._023_CS7NewFeatures
{
    public class _003_DigitalSeparator
    {
        public static void Main(string[] args)
        {
            int binaryData = 0B0010_0111_0001_0000;
            //Will show output as decimal without _
            Console.WriteLine($"{binaryData}");
            Console.ReadLine();
        }
    }
}
