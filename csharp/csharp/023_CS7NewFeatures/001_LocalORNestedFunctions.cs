﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._023_CS7NewFeatures
{
    public class _001_LocalORNestedFunctions
    {
        public static void Main(string[] args)
        {
            void ShowWelcomeMessage()
            {
                Console.WriteLine("Message from local / nested function ");
            }
            ShowWelcomeMessage();
            Console.ReadLine();
        }
    }
}
