﻿using System;
using System.Threading.Tasks;

namespace csharp._003_asyncawait
{
    /* Concurrency 
     * -- Can help in saving time by running parts of application in parallel . 
     * -- When we have to wait, For Ex. to perform I/O bound or compute bound operations, concurrency cannot help. 
     * -- Blocking threads while waiting is not optimal use of threads. 
     * -- We should block threads only when absolutely inevitable For Ex. In case of compute bound operation
     * Synchronous approach - 
     * -- Write the time consuming operations as methods and then call these methods in separate threads or tasks to initiate concurrency. 
     * -- Thread is created and blocked upfront irrespective of whether the operation is I/O bound or compute bound. In case of I/O bound operation, thread is not required and is unnecessarily blocked. 
     * -- Worker thread will have more code which will not have much to do with the expensive part of the operation . Ex business logic around the expensive operation
     * 
     * Asynchronous approach - Optimizing thread utilization by not creating threads for I/O bound operations.
     * --Write the time consuming operation as a method and initiate concurrency inside the method. 
     * -- Threads are not created and blocked upfront. 
     * -- In case of I/O bound operation no thread is created and blocked.  
     * -- For compute bound operation, this approach is not useful as thread will anyways be blocked for this operation.Ex. await Task.run(()=>{Do some thing cpu bound})
     * -- Concurrent part will only have code relevant to the expensive part of the operation.
     */
    class _001_Simpleasaw
    {
        static void Main(string[] args)
        {
            var fileData = ReadFromFile().Result; // No await required when using Result.
            Console.WriteLine(fileData);
            Console.ReadLine();
        }

        private static async Task<string> ReadFromFile()
        {
            TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();
            Task<string> filedatatask = tcs.Task;
            string fileData = System.IO.File.ReadAllText(@"D:\JUnk\DataAccessLogOutput.txt");
            tcs.SetResult(fileData);//Transitions the underlying Task<TResult> into the RanToCompletion state.
            await filedatatask; // Await has to happen on a Task (cincluding one generared using TCS )
            return filedatatask.Result;
        }


        //async ==> method can use the await keyword.
        // await ==> informs compiler that processing cannot continue further till the
        //awaited method completed execution . 
        //Control returns to the caller of the async method.

        private static async Task<string> GetMessage()
        {
            string s = await Task.Run(() => // This represents compute bound as Thread is allocated from thread pool 
            {
                return "Hello World";
            });
            return s;
        }
    }
}
