﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._003_asyncawait
{
    class _002_simpleasaw
    {
        static void Main1(string[] args)
        {
            _002_simpleasaw s = new _002_simpleasaw();
            s.DoSomeTask();
            while (true)
            {
                Thread.Sleep(2000);
                Console.WriteLine("Main thread running ");
            }
        }
        private async Task DoSomeTask() {
            await Task.Run(() => {
                for (int i = 0; i < 50000; i++)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("I = " + i);
                }
            });
            
        }
    }
}
