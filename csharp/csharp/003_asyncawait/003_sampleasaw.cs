﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._003_asyncawait
{
    class _003_sampleasaw
    {
        static void Main1(string[] args)
        {
            Task t1 = DoFirstTask();
            Task t2 = DOSecondTask();
            t1.Wait(); t2.Wait();
            Console.WriteLine("Main completed ");
            Console.ReadLine();

        }

        private static async Task DOSecondTask()
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < 5; i++)
                {
                    Thread.Sleep(1000);
                    Console.WriteLine("Thread 1 executing {0} times ", i);
                }
            });
        }

        private static async Task DoFirstTask()
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < 5; i++)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Thread 2 executing {0} times ", i);
                }
            });
        }

    }
}
