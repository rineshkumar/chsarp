﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _010_TaskParentChild
    {
        static void Main(string[] args)
        {
            Task t = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("Parent task executing.");
                //Starting child task 1 
                Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("child task  1 executing.");
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                    Console.WriteLine("child  task 1 executed.");

                });
                //Starting child task 2 

                Task.Factory.StartNew(() =>
                {
                    Console.WriteLine("child task  2 executing.");
                    Thread.Sleep(TimeSpan.FromSeconds(4));
                    Console.WriteLine("child  task 2 executed.");
                }, TaskCreationOptions.AttachedToParent);

            });
            t.Wait();
            Console.WriteLine("Parent task executed.");
            Console.ReadLine();
        }
    }
}
