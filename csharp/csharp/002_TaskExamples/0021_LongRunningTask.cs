﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    /*
        For long running tasks ,we should not use thread from thread pool 
        For this we can use task creating options which is available only through task.factory.startnow

        For I/O bound operation, we should use asynchronous function / task completion source for implementing concurrency using callbacks. 

        For compute bound operation , to prevent over subscription and hurh performance because of context switching , we will have to throttle concurrency. - Page 953 
    */
    class _0021_LongRunningTask
    {
        static void Main(string[] args)
        {
            Task.Run(() => { return "Hello World"; });

            Task.Factory.StartNew(() =>
            {
                System.Console.WriteLine("Long running task.Is thread from thread pool" + Thread.CurrentThread.IsThreadPoolThread);
                Thread.Sleep(TimeSpan.FromSeconds(10));
            }, TaskCreationOptions.LongRunning);
            Console.ReadLine();
        }
    }
}
