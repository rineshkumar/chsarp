﻿using System;
using System.Threading.Tasks;

namespace csharp.TaskExamples
{
    /*
    Differences between Threads and Tasks 
    1. Getting return value from Threads requires using shared data.
    2. Exceptions which happen in a thread cannot be propogate to the calling thread. 
    3. We cannot configure a thread to start some thing else when its done. Caller thread has to Join this thread due to which it will be blocked. 
    Tasks 
        -- Represents Concurrent Operations 
        -- Operations -- Fine grained , CPU Bound , IO Bound 
        -- For Fine grained concurreny operations 
            -- Use thread from ThreadPool 
        -- For CPU bound operations 
            -- use threads not from threadpool 
        -- For I/O bound operations 
            -- DO not use threads. 
    Tasks --
     1. High level abstractions . 
     2. Represents a concurrent operation which may or may not be backed by thread. 
     3. They are compositinal and can be chained together using continuation.
     4. They can use thread pool
     5. They can use TaskCompletionSource to avoid thread all together. 
     6. To start a Task backed by Thread we use Task.Run
     7. They use thread pool by default => Background thread used. Calling Program need to be blocked from exiting till background thread completes
    */
    class _001_SimpleTask
    {
        static void Main(string[] args)
        {
            Task t1 = Task.Run(() => { Console.WriteLine("simple task "); });
            Console.ReadLine();
        }
    }
}
