﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _012_TaskContinuationException
    {
        static void Main1(string[] args)
        {
            Task t = Task.Run(() => { throw new ArgumentException(); });
            t.ContinueWith((pt) => { 
                if(pt.IsFaulted){
                    Console.WriteLine("faulted");
                }else if(pt.IsCanceled){
                        Console.WriteLine("cancelled");
                }else{
                    Console.WriteLine("COmpleted");
                }
            });
            Console.ReadLine();
        }
    }
}
