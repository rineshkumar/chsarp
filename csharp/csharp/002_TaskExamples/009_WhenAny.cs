﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _009_WhenAny
    {
        static void Main1(string[] args)
        {
            Task<string> t1 = Task.Run(() => {
                Task.Delay(2);
                return "Hello ";

            });

            Task<string> t2 = Task.Run(() =>
            {
                Task.Delay(2);
                return "World ";

            });

            var result = Task.WhenAny(t1, t2);
            Console.WriteLine(result.Result.Result);
            Console.ReadLine();
        }
    }
}
