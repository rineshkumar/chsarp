﻿using System;
using System.Threading.Tasks;

namespace csharp.TaskExamples
{
    class _003_TaskWithException
    {
        /*
         * Tasks can propagate exception which can be caught as aggregate exceptions and can be handled 
         * For an exception happening in Task to be caught, task needs to be blocked using Wait() or Result . 
         * We can use Task.IsFauled / Task.IsCancelled to check what happened. 
         */
        static void Main(string[] args)
        {
            try
            {
                Task<int> t1 = Task.Run(() => { throw new ArgumentException(); return 10; });
                Console.WriteLine(t1.Result);

            }
            catch (AggregateException ex)
            {

                ex.Handle((x) =>
                {
                    Console.WriteLine("Exception " + x.Message);
                    return false;
                });

            }
            Console.ReadLine();

        }
    }
}
