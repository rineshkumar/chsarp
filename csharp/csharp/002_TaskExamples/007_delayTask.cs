﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.TaskExamples
{
    class _007_delayTask
    {
        static void Main1(string[] args)
        {
            Task.Delay(5000).ContinueWith((pt)=>{
                Console.WriteLine("Starting after 5 seconds");
            });
            Console.ReadLine();
        }
    }
}
