﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _008_WhenALl
    {
        static void Main1(string[] args)
        {
            Task<string> t1 = Task.Run(() => {
                Task.Delay(3000);
                return "hello";
            });
            Task<string> t2 = Task.Run(() =>
            {
                Task.Delay(2000);
                return "world";
            });
            Task.WhenAll(t1, t2);
            Console.WriteLine(t1.Result + " " + t2.Result);
            Console.ReadLine();
        }

    }
}
