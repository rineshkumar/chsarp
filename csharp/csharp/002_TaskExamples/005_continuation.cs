﻿using System;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _005_continuation
    {
        static void Main1(string[] args)
        {
            Task<String> t1 = Task.Run(() => { return "Hello "; });
            t1.ContinueWith((pt) =>
            {
                Console.WriteLine(pt.Result + " world");
            });
            Console.ReadLine();
        }
    }
}
