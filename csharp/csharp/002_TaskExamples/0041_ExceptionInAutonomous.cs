﻿using System;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _0041_ExceptionInAutonomous
    {
        static void Main(string[] args)
        {
            TaskScheduler.UnobservedTaskException += HandleUnObseredException();
            Task.Run(() => { throw new ArgumentException(); });
            Console.ReadLine();

        }

        private static EventHandler<UnobservedTaskExceptionEventArgs> HandleUnObseredException()
        {
            Console.WriteLine("Exception caught");
            return null;
        }
    }
}
