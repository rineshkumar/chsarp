﻿using System;
using System.Threading.Tasks;

namespace csharp.TaskExamples
{
    class _002_TaskWithReturn
    {
        /*
         * Return type with be Task<T> where T is the return type. 
         * TO get the return value use 
         * Task<T>.Result. 
         * Task<T>.Result , Task.Wait blocks the calling thread. 
         */
        static void Main1(string[] args)
        {
            Task<string> taskWithReturn = Task.Run(() => { return "Hello World"; });
            Console.WriteLine(taskWithReturn.Result);
            Console.ReadLine();
        }
    }
}
