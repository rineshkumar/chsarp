﻿using System;
using System.Threading.Tasks;
using System.Timers;

namespace csharp._002_TaskExamples
{
    class _006_TaskCompletionSource
    {
        /*
         * When we use a task to represent an operation which is going to start after some time, then we are blocking the underlying thread for that much time. 
         * To represent the operation which is going to start in future without blocking a thread and using the capabilities of task like return values, exception handling, composability(using continuations). 
         * Using TCS we are able to create a Task for operation which is going to start in future without blocking a Thread. 
         * 1. We are not blocking any threads. 
         * 2. Useful for I/O operations which may not start immediately. 
         */
        static void Main(string[] args)
        {
            //TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            var timer = new Timer(2000);// Representing an operation which will start in future. 
            timer.Elapsed += delegate
            {
                timer.Dispose();
                var tcs = new TaskCompletionSource<int>();
                var t1 = tcs.Task; // Slave task which will not block a thread and will represent an operation which will start in future.

                tcs.SetResult(/*Some operation which will start in future*/new Random().Next());
                t1.ContinueWith((pt) => // Now a thread is engaged 
                {
                    Console.WriteLine(t1.Result);
                });
            };
            timer.Start();
            Console.ReadLine();
        }

        private static void CreateNewTaskUsingResult(TaskCompletionSource<int> tcs)
        {
            Task<int> t1 = tcs.Task; // This task does not have to wait in case the operation is going to start in future. 


        }
    }
}
