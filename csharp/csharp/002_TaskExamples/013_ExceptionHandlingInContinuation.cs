﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _013_ExceptionHandlingInContinuation
    {
        static void Main1(string[] args)
        {
            Task t1 = Task.Run(() =>
            {
                throw new ArgumentNullException();
            });
            t1.ContinueWith((pt) =>
            {
                Console.WriteLine(pt.Exception.Message);
            }, TaskContinuationOptions.OnlyOnFaulted);
            t1.ContinueWith((pt) =>
            {
                Console.WriteLine("All is well ");
            },TaskContinuationOptions.OnlyOnRanToCompletion);
            Console.ReadLine();
        }

    }
}
