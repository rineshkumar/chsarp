﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _014_MultipleAntecedentExceptions
    {
        static void Main1(string[] args)
        {
            List<Task> taskList = new List<Task>(); 
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1);
                taskList.Add(Task.Run(() => { throw new ArgumentException(); }));
            }
            Task.WhenAll(taskList).ContinueWith((ant) => {
                foreach (var item in ant.Exception.InnerExceptions)
                {
                    Console.WriteLine(item.Message);
                }
            },TaskContinuationOptions.OnlyOnFaulted);
            Console.ReadLine();
        }
    }
}
