﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _011_CancellationTokenSource
    {
        static void Main1(string[] args)
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            var token = cts.Token;
            var t = Task.Run(() =>
            {
                Console.WriteLine("Running the task ");
                Thread.Sleep(3000); // This task is taking more time to complete 
                if (token.IsCancellationRequested)
                {
                    token.ThrowIfCancellationRequested();
                }

            }, token);
            Thread.Sleep(2000);
            cts.Cancel();
            try
            {
                t.Wait();
            }
            catch (AggregateException ae)
            {

                foreach (var ie in ae.InnerExceptions)
                {
                    Console.WriteLine(ie.Message + " " + t.Status);
                }
            }
            Console.ReadLine();
        }
    }
}
