﻿using System;
using System.Threading.Tasks;

namespace csharp._002_TaskExamples
{
    class _004_autonomousTask
    {
        static void Main1(string[] args)
        {
            Task.Run(() => { Console.WriteLine("Autonomous task . no wait . no result . no continuation. no exception handling "); });
            Console.ReadLine();
        }
    }
}
