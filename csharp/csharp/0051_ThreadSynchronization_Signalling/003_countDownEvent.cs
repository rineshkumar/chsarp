﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._0051_ThreadSynchronization_Signalling
{
    class _003_countDownEvent
    {
        static CountdownEvent GateWithMultipleKey = new CountdownEvent(10);
        static void Main(string[] args)
        {

            for (int i = 0; i < 10; i++)
            {
                int temp = i;
                Task.Run(() =>
                {
                    WorkOnSharedResource(temp);
                });
            }

            GateWithMultipleKey.Wait();
            Console.WriteLine("Main thread received data from all worker thread ");
            Console.ReadLine();
        }

        private static void WorkOnSharedResource(int temp)
        {
            Console.WriteLine("Thread {0} working in shared area", temp);
            Thread.Sleep(2000);
            Console.WriteLine("Thread {0} exiting shared area", temp);
            GateWithMultipleKey.Signal();
        }
    }
}
