﻿using System;

namespace csharp._0051_ThreadSynchronization_Signalling
{
    class _006_LazyInitialization
    {
        /// private static string _expensiveResource;
        private static Lazy<string> _expensiveResource;

        public static string ExpensiveResource
        {
            get
            {
                if (_expensiveResource == null)
                {
                    //LazyInitializer.EnsureInitialized(ref _expensiveResource,
                    //() => new Expensive());
                    _expensiveResource = new Lazy<string>(() =>
                    {
                        return "Expensive resource ";
                    });
                }
                return _expensiveResource.Value;
            }

        }

        static void Main(string[] args)
        {
            Console.WriteLine("Press enter to load the expensive resource");
            Console.ReadLine();
            var s = ExpensiveResource;
            Console.WriteLine(s);
            Console.ReadLine();
        }
    }
}
