﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._0051_ThreadSynchronization_Signalling
{
    class _004_Barrier
    {

        static Barrier ProjectWithMilestones = new Barrier(3);
        static void Main(string[] args)
        {
            for (int i = 0; i < 3; i++)
            {
                int temp = i;
                Task t = Task.Run(() =>
                {
                    WorkOnAlgorithm(temp);
                });
                //                b.AddParticipant();
            }
            Console.Read();
        }

        private static void WorkOnAlgorithm(int temp)
        {

            Console.WriteLine("Thread {0} Working on phase 1 ", temp);
            Thread.Sleep(2000);
            ProjectWithMilestones.SignalAndWait();
            Console.WriteLine("Thread {0}  Working on phase 2 ", temp);
            //Task.Delay(5000);
            ProjectWithMilestones.SignalAndWait();
            Console.WriteLine("Algo completed ");
        }
    }
}
