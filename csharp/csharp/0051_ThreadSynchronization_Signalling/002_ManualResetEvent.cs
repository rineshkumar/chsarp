﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._0051_ThreadSynchronization_Signalling
{
    class _002_ManualResetEvent
    {
        private static ManualResetEvent GateWithSingleKey = new ManualResetEvent(false);
        static void Main(string[] args)
        {
            StartFewThreads(1, 2);
            Console.WriteLine("Press enter to allow all waiting threads to enter shared area");
            Console.ReadLine();
            GateWithSingleKey.Set();
            Thread.Sleep(1000);
            GateWithSingleKey.Reset();// send false signal
            StartFewThreads(3, 4);
            Console.WriteLine("Press enter to allow all waiting threads to enter shared area");
            Console.ReadLine();
            GateWithSingleKey.Set();
            Console.ReadLine();
        }

        private static void StartFewThreads(int start, int end)
        {
            for (int i = start; i <= end; i++)
            {
                int temp = i;
                Task.Run(() =>
                {
                    WorkOnSharedResource(temp);
                });
            }
        }

        private static void WorkOnSharedResource(int temp)
        {
            Console.WriteLine("Thread {0} waiting to enter shared area ", temp);

            GateWithSingleKey.WaitOne();
            Console.WriteLine("Thread {0} working on resoure  ", temp);
            Thread.Sleep(2000);
            Console.WriteLine("Thread {0} exiting shared area  ", temp);
        }
    }
}
