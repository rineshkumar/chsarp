﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._0051_ThreadSynchronization_Signalling
{
    class _005_BarrierWithPostAction
    {
        static Barrier ProjectWithMilestones = new Barrier(3, (b) => { Console.WriteLine("Phase {0} completed ", b.CurrentPhaseNumber); });
        static void Main(string[] args)
        {
            for (int i = 0; i < 3; i++)
            {
                int temp = i;
                Task.Run(() =>
                {
                    RunAlgorithm(temp);
                });
            }
            Console.ReadLine();
        }

        private static void RunAlgorithm(int temp)
        {
            Console.WriteLine("Thread {0} working on phase 1 of algorithm ", temp);
            Thread.Sleep(2000);
            ProjectWithMilestones.SignalAndWait();
            Console.WriteLine("Thread {0} working on phase 2 algorithm ", temp);
            ProjectWithMilestones.SignalAndWait();
        }
    }
}
