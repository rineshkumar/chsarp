﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace csharp._0051_ThreadSynchronization_Signalling
{

    class _001_AutoResetEvent
    {
        private static AutoResetEvent ticketTurnstile = new AutoResetEvent(false);
        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++)
            {
                int temp = i;
                Task.Run(() =>
                {
                    WorkOnTheSharedResource(temp);
                });
            }

            ticketTurnstile.Set();
            Console.ReadLine();
        }

        private static void WorkOnTheSharedResource(int temp)
        {
            Console.WriteLine("Thread {0} waiting to update the resource ", temp);
            ticketTurnstile.WaitOne();
            Console.WriteLine("Thread {0} updating the resource ", temp);
            Thread.Sleep(2000);
            ticketTurnstile.Set();
            Console.WriteLine("Thread {0} exited  ", temp);
        }
    }
}
