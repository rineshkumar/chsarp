﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace csharp._014_BinarySerializer
{
    [Serializable]
    public class Person
    {
        public string Name { get; set; }
        public DateTime BirthDay { get; set; }
        public bool IsEmployed { get; set; }

        [NonSerialized]
        private string _doNotSerialize;

        [OptionalField]
        public int _optional;

        [OnSerializing]
        public void OnSerializing(StreamingContext sc)
        {
            Console.WriteLine("OnSerializing");
        }
        [OnSerialized]
        public void OnSerialized(StreamingContext sc)
        {
            Console.WriteLine("OnSerialized");
        }
        [OnDeserializing]
        public void OnDeserializing(StreamingContext sc)
        {
            Console.WriteLine("OnDeserializing");
        }
        [OnDeserialized]
        public void OnDeserialized(StreamingContext sc)
        {
            Console.WriteLine("OnDeserialized");
        }
    }
    class _001_SimpleBinarySerialization
    {
        static void Main1(string[] args)
        {
            WriteObject();
            ReadObject();
            Console.WriteLine("Done ");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
            IFormatter formatter = new BinaryFormatter();
            Person p;
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\BS_Simple.txt", FileMode.Open))
            {
                p = (Person)formatter.Deserialize(s);
                Console.WriteLine("{0}--{1}--{2}", p.Name, p.BirthDay, p.IsEmployed);
            }
        }

        private static void WriteObject()
        {
            IFormatter formatter = new BinaryFormatter();
            Person p = new Person { Name = "test", BirthDay = DateTime.Now, IsEmployed = true };
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\BS_Simple.txt", FileMode.Create))
            {
                formatter.Serialize(s, p);
            }
        }
    }
}
