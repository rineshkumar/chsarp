﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace csharp._014_BinarySerializer
{
    [Serializable]
    public class EntityUsingISerializable : ISerializable{
        public int SomeNumber { get; set; }
        public EntityUsingISerializable(SerializationInfo info, StreamingContext sc)
        {
            Console.WriteLine("Constructor called");
            SomeNumber = info.GetInt32("name");
        }

        public EntityUsingISerializable()
        {
            // TODO: Complete member initialization
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            Console.WriteLine("GetObjectData called");
            info.AddValue("name", SomeNumber);
        }
    }
    class _002_BS_ISerializable
    {
        static void Main1(string[] args)
        {
            WriteObject();
            ReadObject();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void ReadObject()
        {
            EntityUsingISerializable e;
            IFormatter f = new BinaryFormatter();
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\BS_Simple2.txt", FileMode.Open))
            {
                e  = (EntityUsingISerializable)f.Deserialize(s);
                Console.WriteLine(e.SomeNumber);
            }
        }

        private static void WriteObject()
        {
            EntityUsingISerializable e = new EntityUsingISerializable { SomeNumber = 10};
            IFormatter f = new BinaryFormatter();
            using (Stream s = new FileStream(@"C:\Users\rinesh_kumar\Documents\BS_Simple2.txt", FileMode.Create))
            {
                f.Serialize(s, e);
            }
        }
    }
}
