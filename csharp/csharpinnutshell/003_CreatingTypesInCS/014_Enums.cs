﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    //enum BorderSide : byte { Left = 10, Right = 11, Bottom = 12, Top = 14 }
    enum BorderAlinment { Left = BorderSide.Left, Right = BorderSide.Right, Center }
    [Flags]
    enum BorderSide { Left = 0, Right = 1, Bottom = 2, Top = 4 }
    class _013_Enums
    {
        static void Main(string[] args)
        {
            BorderSide topSide = BorderSide.Top;
            //System.Console.WriteLine(topSide == BorderSide.Top);
            //casting between enum and underlying integer value. 
            //int i = (int)BorderSide.Left;
            //BorderSide side = (BorderSide)i;
            //System.Console.WriteLine(side == BorderSide.Left);

            //BorderAlinment ba = (BorderAlinment)BorderSide.Left;
            //Console.WriteLine(ba == BorderAlinment.Left);

            BorderSide leftRight = BorderSide.Left | BorderSide.Right;
            var includesRight = (leftRight & BorderSide.Right) != 0;//true 
            Console.WriteLine(includesRight);
            Console.Read();
        }
    }
}
