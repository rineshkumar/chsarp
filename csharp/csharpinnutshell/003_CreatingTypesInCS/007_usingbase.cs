﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    /*
     * 1.Complete subclass field initialization . 
        2. Call base constructor .
        3. Complete base class field initialization . 
        4. Call base class constructor method. 
        5.Call sub class constructor 
     */
    public class BaseClas1s
    {

        public virtual string Message { get { return "Hello from base "; } }
        public string Message2 = "Message 2 from base ";

    }
    public class DerivedClass1 : BaseClas1s

    {
        public override string Message => "Hello from derived ";
        public string Message2 = "Message 2 from derived ";
        public void ShowCombinedMessages()
        {
            System.Console.WriteLine(base.Message + " " + Message); ///Getting the overridden member
            Console.WriteLine(base.Message2); //Getting the hidden member
        }
        static void Main(string[] args)
        {
            DerivedClass1 dc = new DerivedClass1();
            dc.ShowCombinedMessages();
            Console.Read();
        }
    }
}
