﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    public class BaseClass
    {
        public virtual void SayHello()
        {
            Console.WriteLine(" Base Hello");
        }

    }
    public class Overrider : BaseClass
    {
        public override void SayHello()
        {
            Console.WriteLine(" Overrider Hello");
        }
    }
    public class Hider : BaseClass
    {
        public void SayHello()
        {
            base.SayHello();
        }
    }
    class HidingDemo
    {

        static void Main(string[] args)
        {
            BaseClass bc = new BaseClass();
            Overrider oc = new Overrider();
            Hider hc = new Hider();
            bc = oc;
            bc.SayHello(); // Type of instance considered --- OverRider hellp 
            bc = hc;
            bc.SayHello(); // Type of reference considered -- base Hello 
            Console.Read();
        }
    }
}
