﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    class _008_OverLoading
    {
        static void Main(string[] args)
        {
            House h = new House { Name = "House" };
            Stock s = new Stock { Name = "Stock" };
            Display(h);
            Display(s);
            Asset a = h;
            Display(a); //  The over load to call is determined statically
            //i.e based on the type of the reference and not the original object 
            Console.Read();
        }

        private static void Display(Asset a)
        {
            Console.WriteLine("Displaying asset");
        }

        private static void Display(Stock s)
        {
            Console.WriteLine("Displaying stock ");
        }

        private static void Display(House h)
        {
            Console.WriteLine("Displaying house ");
        }
    }
}
