﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    class _005_ClassUsingInheritance
    {
        static void Main(string[] args)
        {
            Asset a;// = new Asset { Name = "House" };
            //Display(a);
            Stock s = new Stock { Name = "Stocks ", SharesOwned = 10 };
            Display(s);
            House h = new House
            {
                Name = "house",
                Mortgage = 20
            };
            Display(h);
            //Upcasting 
            a = s;
            //Downcasting -- Requires explicit casting  
            s = (Stock)a;

            a = h;
            s = (Stock)a;// This will fail at run time 
            //Checks before casting 
            if (a is Stock)
                s = a as Stock;

            Console.Read();
        }

        private static void Display(Asset a)
        {
            Console.WriteLine("Asset Name " + a.Name);
        }
    }
}
