﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    /*
     * generic constraints 
     * 
        where T : base-class // Base-class constraint
        where T : interface // Interface constraint
        where T : class // Reference-type constraint
        where T : struct // Value-type constraint (excludes Nullable types)
        where T : new() // Parameterless constructor constraint
        where U : T // Naked type constraint

     */
    class _016_generics
    {
        static void Main(string[] args)
        {
            //SomeClass sc = new SomeClass();
            //int a = 10, b = 20;
            //sc.Swap(ref a, ref b);
            //System.Console.WriteLine("a = {0},b = {1}", a, b);
            //SomeGenericClass<int> sc = new SomeGenericClass<int>();//Closed generic type. 
            //var openType = typeof(SomeGenericClass<>); // Type of  open generic type .
            //var closedType = typeof(SomeGenericClass<int>); // type of closed generic type . 
            //var val = default(string);//default(T);//Can be used for getting default of a type parameter.


            SomeClassWithStaticData<int>.count = 10;
            SomeClassWithStaticData<string>.count = 7;
            SomeClassWithStaticData<float>.count = 5;
            Console.WriteLine("{0},{1},{2}",
                SomeClassWithStaticData<int>.count,
                SomeClassWithStaticData<string>.count,
                SomeClassWithStaticData<float>.count

                );
            Console.Read();
        }
    }
    public class SomeGenericClass<T> { }//Open generic type 
    public class SomeSubClass<T> : SomeGenericClass<T> { } // Type parameter open
    public class SomeSubClass2 : SomeGenericClass<int> { } // type parameter closed.
    /*
     * Typesafe -- using Type parameter it has communcated rule that it can used only for this specific type.
     * boxning / unboxing - No boxing and unboxing happening and hence no casting issues.
     */
    public class TypeSafeStack<T>
    {
        T[] data;
        int _position;

        public TypeSafeStack(int capacity)
        {
            data = new T[capacity];
        }
        public void Push(T element)
        {
            data[_position++] = element;
        }
        public T Pop()
        {
            return data[_position--];
        }
    }
    public class NonTypeSafeStack
    {
        object[] data = new object[100];
        int position;
        public NonTypeSafeStack(int capacity)
        {
            data = new object[capacity];
        }
        public void Push(object o)
        {
            data[position++] = o;
        }
        public object Pop()
        {
            return data[position--];
        }
        static void Main(string[] args)
        {
            NonTypeSafeStack s = new NonTypeSafeStack(50);
            //Boxing is happening . 
            s.Push(1);
            s.Push("Rinesh");
            s.Push(10.0);
            s.Push(true);
            //For unboxing downcasting is required. 
            //CAn lead to casting errors. 

            var value = (char)s.Pop();// Casting done. can throw error. 
            //We need 
            //1.Reusable code. 
            //2. Type safety ==> Type has certain rules in place based on which we use it. Here we can push element of any type. 
            //3.Less of boxing and unboxing. 
        }
    }
    public class SomeClass
    {
        /*
         * Generics being used for fundamental algorithms 
         */
        public void Swap<T>(ref T a, ref T b)
        {
            T temp = a;
            a = b;
            b = temp;
        }
    }

    public interface ISomeInterface<T>
    {
        void DoSomething(T obj);
    }
    public class SomeClass2 : ISomeInterface<SomeClass2>
    {
        public void DoSomething(SomeClass2 obj)
        {
            throw new NotImplementedException();
        }
    }
    public class SomeClassWithStaticData<T>
    {
        public static int count;
    }

}
