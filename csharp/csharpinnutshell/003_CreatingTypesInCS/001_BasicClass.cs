﻿using csharpinnutshell._003_CreatingTypesInCS;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    /*
     * Before keyword class - Attributes & Class modifiers. 
     * public - Accessible from anywhere 
     * private - cannot be private 
     * protected - Cannot be protected. 
     * internal - Default accessability 
     * abstract - Cannot be instantiated. 
     * sealed - cannot be inherited . 
     * static - All methods are static. Cannot be referene through an instance
     * 
     * unsafe 
     * partial classes - Definition of the class lies in multiple files.
     * 
     */
    class _001_BasicClass
    {
        /*
         * Access modifier types
         * For Class 
         * --> Class Related -- Static 
         * --> Inheritance related -- Abstract 
         * --> Read only related -- Sealed
         * --> Member related - Public , Private (NA), Protected (NA), internal , partial 
         *  --> unsafe code related -- unsafe 
         * --> Thread related - NA 
         * 
         * For Fields 
         * --> Class related -- Static 
         * --> Inheritance related -- new 
         * --> Read only related -- read only 
         * --> Member related -  public , private , protected, internal .
         * --> unsafe code related -- unsafe 
         * Thread related -- volatile 
         * 
         * For Methods 
         * --> Class related -- Static
         * --> Inheritance related --> new virtual , abstract , override , 
         * --> Read only related -- sealed 
         * --> Member related --> public private protected internal partial . 
         * --> unsafe code related -- unsafe, extern 
         * --> Thread related - NA 
         */
        //----------------Fields 
        public int _publicField = 0;
        private int _privateField = 10;
        protected int _protectedField = 20;
        internal int _internalField = 10;
        //-->inside Class main method 
        //----> Without Instance
        //------> Public -- Not accessible
        //------> Private -- Not accessible
        //------> Protected -- Not accessible
        //------> internal -- Not accessible

        //----> With Instance 
        //------> Public -- Accessible
        //------> Private -- Accessible
        //------> Protected -- Accessible
        //------> internal -- accessible

        //--> Inside same package - Non Subclass
        //------> Without Instance 
        //--------> Public -- Not accessible
        //--------> Private -- Not accessible
        //--------> Protected -- Not accessible
        //------> internal -- Notaccessible

        //------> With Instance 
        //--------> Public -- Accessible
        //--------> Private -- Not Accessible 
        //--------> Protected -- not Accessible
        //------> internal -- accessible

        //--> Inside same package - Subclass
        //------> Without Instance 
        //------> Public -- accessible
        //------> Private -- Not accessible
        //------> Protected -- accessible
        //------> internal -- accessible
        //------> With Instance 
        //------> Public -- Accessible
        //------> Private -- Not Accessible 
        //------> Protected -- Not Accessible
        //------> internal -- accessible

        //--> Inside different package -  Non Subclass
        //------> Without Instance 
        //------> Public -- Not accessible
        //------> Private -- Not accessible
        //------> Protected -- Not accessible
        //------> internal -- Not accessible
        //------> With Instance 
        //------> Public -- Accessible
        //------> Private -- Not Accessible 
        //------> Protected -- Not Accessible
        //------> internal -- accessible

        //--> Inside different package -  Subclass
        //------> Without Instance 
        //------> Public -- accessible
        //------> Private -- Not accessible
        //------> Protected -- accessible
        //------> internal -- accessible
        //------> With Instance 
        //------> Public -- Accessible
        //------> Private -- Not Accessible 
        //------> Protected -- Not Accessible
        //------> internal -- accessible


        //----------------Properties 
        //----------------Constructor 
        //----------------indexers 
        //----------------nested types 

        //----------------methods 
        //----------------Events 
        //----------------Finalizers 

        //----------------overloaded operators
        static void Main(string[] args)
        {
            int publicFieldCopy, privateFieldCopy, protectedFieldCopy, internalFieldCopy;
            //Without instance 
            //publicFieldCopy = _publicField;//Not accessible 
            //protectedFieldCopy = _protectedField;//Not Accessible
            //privateFieldCopy = _privateField;//Not accessible 
            //internalFieldCopy = _internalField;
            //With instance

            _001_BasicClass bc = new _001_BasicClass();
            publicFieldCopy = bc._publicField;
            protectedFieldCopy = bc._protectedField;
            privateFieldCopy = bc._privateField;


            internalFieldCopy = bc._internalField;
        }
    }
    class SamePackageNonSubClass
    {
        public void CheckAccessability()
        {
            int publicFieldCopy, privateFieldCopy, protectedFieldCopy, internalFieldCopy;
            //Without instance 
            //publicFieldCopy = _publicField; 
            //protectedFieldCopy = _protectedField;
            //privateFieldCopy = _privateField;
            //internalFieldCopy = _internalField

            //With instance

            _001_BasicClass bc = new _001_BasicClass();
            publicFieldCopy = bc._publicField;// Accessible
            //protectedFieldCopy = bc._protectedField;
            //privateFieldCopy = bc._privateField;


            internalFieldCopy = bc._internalField;
        }
    }
    class SamepackageSubclass : _001_BasicClass
    {
        public void CheckAccessability()
        {
            int publicFieldCopy, privateFieldCopy, protectedFieldCopy, internalFieldCopy;
            //Without instance 
            publicFieldCopy = _publicField;
            protectedFieldCopy = _protectedField;
            //privateFieldCopy = _privateField;
            internalFieldCopy = _internalField;
            //With instance

            _001_BasicClass bc = new _001_BasicClass();
            publicFieldCopy = bc._publicField;// Accessible
            //protectedFieldCopy = bc._protectedField;
            //privateFieldCopy = bc._privateField;


            internalFieldCopy = bc._internalField;
        }
    }
}
namespace someothernamespace
{
    class DifferentPackageNonSubclass
    {
        public void CheckAccessability()
        {
            int publicFieldCopy, privateFieldCopy, protectedFieldCopy, internalFieldCopy;
            //Without instance 
            //publicFieldCopy = _publicField;//
            //protectedFieldCopy = _protectedField;//
            //privateFieldCopy = _privateField;
            //internalFieldCopy = _internalField;

            //With instance

            _001_BasicClass bc = new _001_BasicClass();
            publicFieldCopy = bc._publicField;//Accessible
            //protectedFieldCopy = bc._protectedField;
            //privateFieldCopy = bc._privateField;


            internalFieldCopy = bc._internalField;
        }
    }
    class DifferentPackageSubClass : _001_BasicClass
    {
        public void CheckAccessability()
        {
            int publicFieldCopy, privateFieldCopy, protectedFieldCopy, internalFieldCopy;
            publicFieldCopy = _publicField;//Accessible
            protectedFieldCopy = _protectedField;//Accessible
            //privateFieldCopy = _privateField;
            //With instance
            internalFieldCopy = _internalField;

            _001_BasicClass bc = new _001_BasicClass();
            publicFieldCopy = bc._publicField;//Accessible
            //protectedFieldCopy = bc._protectedField;
            //privateFieldCopy = bc._privateField;


            internalFieldCopy = bc._internalField;
        }
    }
}
