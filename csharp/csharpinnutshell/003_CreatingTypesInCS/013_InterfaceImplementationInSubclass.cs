﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    public interface IUndoable
    {
        void Undo();
    }
    public class BaseClass2 : IUndoable
    {
        public void Undo()
        {
            System.Console.WriteLine("BaseClass2.Undo");
        }
    }
    public class SubClass : BaseClass2, IUndoable
    {
        public void Undo()
        {
            System.Console.WriteLine("Subclass.Undo");
        }
    }
    class _013_InterfaceImplementationInSubclass
    {
        static void Main(string[] args)
        {
            SubClass sc = new SubClass();
            sc.Undo(); //Subclass.Undo
            ((IUndoable)sc).Undo(); //Subclass.Undo
            ((BaseClass2)sc).Undo();//BaseClass2.Undo
            //Reimplementation hijacking is possible only when the subclass is referred using the interface  and not the base class
            Console.Read();
        }
    }
}
