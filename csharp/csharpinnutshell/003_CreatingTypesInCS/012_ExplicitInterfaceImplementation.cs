﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    interface I1 { void Foo(); }
    interface I2 { int Foo(); }

    class _012_ExplicitInterfaceImplementation : I2, I1
    {
        /*
         * Implicitly implemented interface member is sealed. 
         * Mark it as virtual or abstract for overriding. 
         * Explicitly implemented interface cannot be marked virtual or abstract 
         */
        public int Foo()
        {
            return 1;
        }

        void I1.Foo()
        {
            System.Console.WriteLine("In I1 Foo");
        }
        static void Main(string[] args)
        {
            I1 i1 = new _012_ExplicitInterfaceImplementation();
            i1.Foo();
            I2 i2 = new _012_ExplicitInterfaceImplementation();
            i2.Foo();
            Console.Read();
        }
    }
}
