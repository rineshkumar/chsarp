﻿namespace csharpinnutshell._003_CreatingTypesInCS
{
    abstract class Asset
    {
        public string Name { get; set; }


        public virtual decimal Liability
        {
            get { return 0; }
        }
        public abstract decimal GetNetWorth();

    }
}
