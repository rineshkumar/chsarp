﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    /*
     * Access modifiers 
     * Class Related -- NA 
     * Inheritance Related -- NA 
     * Readonly related -- NA 
     * Member Related - public private protected internal
     * unsafe code related -- unsafe extern 
     * Thrad related - NA 
     */
    class _003_Constructors
    {
        private static int _staticField1 = 10;

        private int _baseField = 50;
        private int _extendedField = 50;
        private int _newField;
        public int AutoProperty { get; set; }
        private string[] words = "Life is beautiful".Split();
        public string this[int index]
        {
            get { return words[index]; }
            set { words[index] = value; }
        }


        /*
         * Const 
         * 1. Value cannot be changed once assigned 
         * 2. Its static and can be accessed using class. 
         * 3. Evaluation happens at compile time. 
         * 4. Compiler replaces the value literally
         * 5. Useful for constants which will never change -- PI 
         * 6. In case its value changed, all dependent assemblies needs to be recompiled.
         */
        const string Message = "Hello World ";
        /*
         * Read only - This is also constant, but if value changes, dependent assemblies need not be recompiled.
         */
        public readonly string Message2 = "Hello World 2 ";

        public int NewField
        {

            get { return _baseField * _extendedField; }
            private set { _newField = value; }
        }

        static _003_Constructors()
        {
            Console.WriteLine("Statix field value " + _staticField1);
            Console.WriteLine("Static constructor called ");
        }
        public _003_Constructors CreateInstance()
        {
            return new _003_Constructors();
        }
        static void Main(string[] args)
        {
            _003_Constructors c = new _003_Constructors { _baseField = 10 };
            System.Console.WriteLine("_baseField after object initialization " + c._baseField);
            Console.Read();
        }
    }
    public static class StaticClass
    {
        //int member; // cannot have static member
        //public void HelloWorld();//can only have static methods.

    }
    //  public static class StaticSubClass : StaticClass { } // Cannot extend a static class
}