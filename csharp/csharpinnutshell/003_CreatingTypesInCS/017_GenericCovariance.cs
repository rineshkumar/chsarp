﻿namespace csharpinnutshell._003_CreatingTypesInCS
{
    /*
     * Covariance -- Its about implicit casting . 
     * If B can be implicity casted to A 
     * Then G is covariant if G<B> can be implicitly converted to G<A>
     generic interface and arrays support covariance. 
     Generic classes do not support covariance. 
     */
    public class Stack<T> // A simple Stack implementation
    {
        int position;
        T[] data = new T[100];
        public void Push(T obj) { data[position++] = obj; }
        public T Pop() { return data[--position]; }
    }
    class Animal { }
    class Bear : Animal { }
    class Camel : Animal { }
    class _017_GenericCovariance
    {
        static void Main(string[] args)
        {
            //Bear can be implicitly converted to Animal . 
            // If Stack is covariant then 
            // variable of type Stack<Animal> can refer one of type Stack<Bear>
            Stack<Bear> bears = new Stack<Bear>();
            Stack<Animal> animals = new Stack<Animal>();
            //animals = bears; // If Stack is covariant. THIS IS NOT 
            //If covariant we can do following also 
            animals.Push(new Camel()); // camel will mix with bears causing casting issues. 
            //Lead to casting issues and reusability issues 


        }
    }
}
