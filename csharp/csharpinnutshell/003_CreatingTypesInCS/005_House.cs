﻿namespace csharpinnutshell._003_CreatingTypesInCS
{
    class House : Asset
    {
        public decimal Mortgage { get; set; }

        public override decimal Liability { get { return Mortgage; } }

        public override decimal GetNetWorth()
        {
            return 10.0m - Liability;
        }
    }
}
