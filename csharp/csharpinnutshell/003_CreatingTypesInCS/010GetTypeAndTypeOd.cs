﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    public class Point { public int X, Y; }
    class _010GetTypeAndTypeOd
    {
        /*
         * typeof is used at compile time 
         * GetType is used at run time 
         */
        static void Main()
        {
            Point p = new Point();
            Console.WriteLine(p.GetType().Name); // Point
            Console.WriteLine(typeof(Point).Name); // Point
            Console.WriteLine(p.GetType() == typeof(Point)); // True
            Console.WriteLine(p.X.GetType().Name); // Int32
            Console.WriteLine(p.Y.GetType().FullName); // System.Int32
        }
    }
}
