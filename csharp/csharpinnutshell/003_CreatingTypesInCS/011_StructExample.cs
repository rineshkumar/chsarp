﻿namespace csharpinnutshell._003_CreatingTypesInCS
{
    struct SampleStruct
    {
        int x;//= 10;//Does not allow field initialization 
        int y;

        //public SampleStruct(int x) // All properties should be assigned 
        //{
        //    this.x = x;
        //}

        //public SampleStruct() // Parameterless construtor not allowed 
        //{
        //}

        public SampleStruct(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
    //struct SubStruct : SampleStruct { } // Deriving from other struct not allowed 
    class _011_StructExample
    {
    }
}
