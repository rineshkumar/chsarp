﻿namespace csharpinnutshell._003_CreatingTypesInCS
{
    class _002_MethodOverloading
    {
        //Method overloading 
        //method name and parameter type considered
        // ref and out considered part of signature 

        public void SomeMethod(int a) { }
        //public void SomeMethod(int b) { }//parameter name not considered 
        //public string SomeMethod(int a) { }// return type not considered
        public void SomeMethod(ref int a) { } // ref and out considered aprt of signature.
        //public void SomeMethod(out int a) { } //ref and out cannot coexist together s


        public void SomeMethod2(int[] a) { }
        //public void SomeMethod2(params int[] a) { }// params not considered 


    }
}
