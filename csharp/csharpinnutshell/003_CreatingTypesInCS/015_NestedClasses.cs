﻿using System;

namespace csharpinnutshell._003_CreatingTypesInCS
{
    class OuterClass
    {
        static int OuterClassMember = 10;
        int NonStaticMember = 20;
        public class InnerClass
        {
            OuterClass o;



            public InnerClass(OuterClass o)
            {
                this.o = o;
            }
            public void printOuterClassMember()
            {

                Console.WriteLine(o.NonStaticMember);
                Console.WriteLine(OuterClassMember);
            }
        }
    }

    class _015_NestedClasses
    {
        static void Main(string[] args)
        {
            ////instantiating inner class
            //sending parent 
            OuterClass oc = new OuterClass();
            OuterClass.InnerClass ic = new OuterClass.InnerClass(oc);
            ic.printOuterClassMember();
            Console.Read();
        }




    }
}
