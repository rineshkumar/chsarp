﻿namespace csharpinnutshell._003_CreatingTypesInCS
{
    class Stock : Asset
    {
        public long SharesOwned { get; set; }

        public override decimal GetNetWorth()
        {
            return SharesOwned * 1.0m;
        }
    }
}
