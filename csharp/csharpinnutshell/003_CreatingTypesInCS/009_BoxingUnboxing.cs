﻿using System;

namespace csharpinnutshell
{
    class _009_BoxingUnboxing
    {
        static void Main(string[] args)
        {
            object i = 10; // Boxing 
            int j = (int)i;//unboxing - requires casting . 

            //Boxing copies the value 
            int k = 10;
            object l = k;
            k = 11;
            System.Console.WriteLine(l + " " + k);
            Console.Read();

        }
    }
}
