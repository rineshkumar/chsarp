﻿using System;

namespace csharpinnutshell._002_CSLanguagebasics
{
    /*
     * Primitive reference assignment - Causes creation of new copies of the object including the fields in the object
     * Value types cannot be assigned null values
     * Memory used is precisely the total of the storage required for the fields.
     */
    public struct Point
    {
        public int X;
        public int Y;
    }
    class _002_PrimitiveTypeReferenceComparison
    {
        static void Main(string[] args)
        {
            Point p1 = new Point(); ;
            p1.X = 10;
            p1.Y = 20;
            Point p2 = p1;
            p2.X = 50;

            System.Console.WriteLine(p1.X);
            System.Console.WriteLine(p2.X); // Will have different value . 
            Console.ReadLine();
        }
    }
}
