﻿using System;

namespace csharpinnutshell._002_CSLanguagebasics
{
    class _0012_General
    {
        static void Main(string[] args)
        {
            //const variables . 
            //it is defined once and its value cannot be changed after that .
            //const int a = 10;
            //a = 20;//Will not allow 
            //-------------------------

            //Local variables and scope
            //int x;
            //{
            //    int x;// Not allowed as there is one in scope.
            //    int y;
            //}
            //{
            //    int y;// No issues with the variable in another scope
            //}
            //Console.WriteLine(y);//y is not in scope

            //-------------------------
            //switch(expression )
            //Expression should be such that it can be statically evaluated.
            // Pritive integral , bool, enum , string 
            //-------------------------

            Console.Read();
        }
    }
}
