﻿namespace csharpinnutshell._002_CSLanguagebasics
{
    /*
     * Class reference assignment - Copy of the object is not made. Copy of the object reference is provided to the new variable. 
     * Memory requirement is total of the storage required for the fields and for the reference itself. 
     */
    class _003_ReferenceTypes
    {
    }
}
