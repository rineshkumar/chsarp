﻿using System;
using System.Linq;

namespace csharpinnutshell._002_CSLanguagebasics
{
    /*
     * Rules for implicit conversion 
     * 1. Compiler is sure that conversion can happen.
     * 2. No information will be lost in the conversion. 
     * Rules for explicit conversion. 
     * 1. Conpiler is not sure that the conversion can happen . 
     * 2. Information will be lost in the conversion . 
     * 
     * Destination type should be able to represent every possible value of source type for implicit conversion.
     */
    class _001_General
    {
        static void Main(string[] args)
        {
            //Casting Rules ------------
            //int i = 10;
            //short s = i; // not sure if conversion can happen. 
            //--------------------------
            //Casting rules 
            //double d = 10.9999999999;
            //i = d;//not sure if the conversion can happen . 
            //--------------------------
            //Casting rules 
            //d = i;//no issues as no data loss
            //--------------------------
            //Casting rules 
            //short a = 10;
            //short b = 10;
            //short c = a * b;// not sure if the value will fit . 
            //--------------------------
            //Casting , truncation , rounding 
            //i = (int)d;
            //double d2 = 10.9999999999;
            //System.Console.WriteLine(i);
            //System.Console.WriteLine(Convert.ToInt32(d2));
            //--------------------------
            //Division by literal or constant will throw comile time error 
            //i = 0;
            //int j = 10 / i; // Will throw run time error 
            //j = 10 / 0; // Will generate compile time error 
            //--------------------------
            //Wrapping around of integers . 
            //i = int.MaxValue;
            //i++;
            //Console.WriteLine(i == int.MinValue);
            //--------------------------
            //Checked exceptions 
            //checked// Will throw overflow exception 
            //{
            //    i = int.MaxValue;
            //    i++;
            //    Console.WriteLine(i == int.MinValue);
            //}
            //--------------------------
            //8 and 16 bit integratal type arithmetic limitation 
            //upcasted to int32 for arithmetic operations. 
            //short a = 10, b = 20;
            //short c = (short)(a + b); // Compile time error . Need casting
            //--------------------------
            //Problem with double 
            //doubles represented in base 2 . Number with fraction not represented properly
            //float one = 1.0f;
            //float oneTenth = .1f;
            //Console.WriteLine(one - 10 * oneTenth);//non zero 
            //--------------------------
            //decimal - represented using base 10 . Fraction part handled well.
            //Good for arithmetic calculations.
            //decimal one = 1.0m;
            //decimal oneTenth = .1m;
            //Console.WriteLine(one - 10 * oneTenth);//precisely 0 
            //--------------------------
            // characters and number conversion.
            //char c = 'A';
            //int a = c;
            //Console.WriteLine(a);//Prints 65 
            //c = (char)65;
            //Console.WriteLine(c);//Prints 65 
            //--------------------------
            //String verbatim 
            //use " to include "
            //string s = @"\\fileSer""verPath";
            //--------------------------
            //Array initialization
            //char[] vowels1;
            //vowels1 = new char[] { 'a', 'e', 'i', 'o', 'u' };
            //vowels1 =  { 'a', 'e', 'i', 'o', 'u' }; // Will not work 

            //declaration and initialiation has to happen in same line . 
            //char[] vowels2 = { 'a', 'e', 'i', 'o', 'u' };
            //--------------------------
            //Rectangular arrays - Uses commas and get length 
            //int[,] rectangularArray = new int[2, 3];
            //for (int i = 0; i < rectangularArray.GetLength(0); i++)
            //{
            //    for (int j = 0; j < rectangularArray.GetLength(1); j++)
            //    {
            //        rectangularArray[i, j] = i * j;
            //    }
            //}
            //for (int i = 0; i < rectangularArray.GetLength(0); i++)
            //{
            //    for (int j = 0; j < rectangularArray.GetLength(1); j++)
            //    {
            //        Console.Write(rectangularArray[i, j]);
            //        Console.Write(" ");
            //    }
            //    Console.WriteLine();
            //}
            //--------------------------
            //Rectangular array initialization 
            // Size of the inner most dimension will be same in all cases. 
            //int[,] rectangularArray = new int[,]
            //{
            //    { 11,12},
            //    { 21,22},
            //    //{ 31,32,33}// Will throw compile time error 
            //    { 31,32}
            //};
            //--------------------------
            //Jagged arrays - square brackets with last one empty 
            // Size of the inner most dimension is not defined and can vary 
            //int[][] jaggedArrray = new int[10][];
            //var random = new Random(); // use the same random all the time. 
            //for (int i = 0; i < jaggedArrray.Length; i++)
            //{
            //    int innerDimension = random.Next(1, 10);

            //    jaggedArrray[i] = new int[innerDimension];// Inner dimension will not be same

            //    for (int j = 0; j < jaggedArrray[i].Length; j++)
            //    {
            //        jaggedArrray[i][j] = innerDimension;
            //    }
            //}
            //for (int i = 0; i < jaggedArrray.Length; i++)
            //{
            //    for (int j = 0; j < jaggedArrray[i].Length; j++)
            //    {
            //        Console.Write(jaggedArrray[i][j]);
            //        Console.Write(" ");
            //    }
            //    Console.WriteLine();
            //}
            //--------------------------
            //Jagged array initialization 2 
            //int[][] jaggedArray = new int[][]
            //{
            //    new int[] { 1,2,3},
            //    new int[] { 1,2},
            //    new int[] { 1,2,3,4}
            //};
            //--------------------------
            //Pass by reference 
            // Default pass by value 
            //Primitive Type - Copy of value 
            // Reference type - Copy of reference 
            // ref to pass original primitive value. 
            //int i = 10;
            //update(ref i);
            //Console.WriteLine(i);// Will give 20 due to pass by reference 
            //--------------------------
            //out -- to return multiple values from methods.
            // need not be initialized. Are passed by reference just like ref 
            //int a, b, c;
            //GiveValuesFor(out a, out b, out c);
            //Console.WriteLine(a + " " + b + " " + c);
            //--------------------------
            //params -- To send variable number of values to a method. 
            //Should be the last parameter and should be an array . 
            //ShowDetails("Rinesh", 10, 20, 30, 40);
            //ShowDetails("Rinesh", 10, 20, 30);
            //ShowDetails("Rinesh", new int[] { 10, 20, 30 });
            //--------------------------
            //Optional Parameters - Need not be send in method calls. 
            // Should appear after mandatory parameters. 
            //ShowValue(10);
            //ShowValue();
            //--------------------------
            //Named arguments - Identifying method parameters by name in place of position . 
            //  ShowNamedValue(b: 10, a: 20);//Use : and not = , == used in object initialization
            //--------------------------
            //Named and optional arguments tigether 
            //For sending some of the optional arguments out of order . 
            //ShowNamedAndOptionalValue(b: 20);
            //--------------------------
            //Implicitly typed local variables. 
            // Typing happens at compile time and once done will not change . 
            //Reduces code readability. 
            //var x = 10;
            //x = "Rinesh";//Compile time error.
            Console.ReadLine();


        }

        private static void ShowNamedAndOptionalValue(int a = 20, int b = 10)
        {
            Console.WriteLine("a = {0}, b = {1}", a, b);
        }

        private static void ShowNamedValue(int a, int b)
        {
            Console.WriteLine("a = {0}, b = {1}", a, b);
        }

        private static void ShowValue(int v = 50)
        {
            Console.WriteLine(v);
        }

        private static void ShowDetails(string v1, params int[] marks)
        {
            Console.WriteLine("Name =" + v1);
            Console.WriteLine(marks.Sum());

        }

        private static void GiveValuesFor(out int a, out int b, out int c)
        {
            a = 10; b = 20; c = 30;
        }

        private static void update(ref int i)
        {
            i = i + 10;
        }
    }
}
